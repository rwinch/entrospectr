/*
 * 
 */
package entrospector;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

/**
 * @author rwinch
 */
public class UtilitiesTest extends TestCase {
  
  private static final Logger logger=Logger.getLogger(UtilitiesTest.class);
  /**
   * This test MAY OCASSIONALLY fail
   */
  public void testRandmizeArray() {
    int size = 1000000;
    int strength = 5;
    // TODO: how do we calculate this?
    int expectedMax = 100;
    
    Object[] array = new Object[size];
    
    // init the array
    for(int i=0;i<array.length;i++) {
      array[i] = new Integer(i);
    }
    
    // Randomize it (there will be strength*size swaps) 
    array = Utilities.randomizeArray(array,strength);
    
    // number of objects in location they started off in
    int count = 0;
    for(int i=0;i<array.length;i++) {
      Integer a = (Integer)array[i];
      if(a!=null && i==a.intValue()) {
        count++;
      }
    }
    if(logger.isDebugEnabled()) {
      logger.debug("expectedMax="+expectedMax+" count="+count);
    }
    assertTrue(expectedMax>count);
  }
}
