/*
 * 
 */
package entrospector;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import entrospector.Utilities;
import entrospector.graph.impl.VertexImpl;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;
import entrospector.io.impl.DirectedUndirectedToUndirected;
import entrospector.io.impl.MalformatedException;
import entrospector.io.interfaces.GraphFileReader;

/**
 * @author rwinch
 */
public abstract class AbstractEntrospectorTest extends TestCase {
  private static final Logger logger = Logger.getLogger(AbstractEntrospectorTest.class);

  protected final static double PRECISSION = 0.0000001;
  
  /**
   * Create some vertices to use
   */
  protected Vertex _1 = VertexImpl.create("1"), 
	_2 = VertexImpl.create("2"),
	_3 = VertexImpl.create("3"),
	_4 = VertexImpl.create("4"),
	_5 = VertexImpl.create("5"),
	_6 = VertexImpl.create("6"),
	_7 = VertexImpl.create("7"),
	_8 = VertexImpl.create("8"),
	_9 = VertexImpl.create("9"),
	_10 = VertexImpl.create("10");
  
  public String getTestFile(String file) {
    String base = Utilities.TEST_DATA_DIRECTORY;
    if(logger.isDebugEnabled()) {
      logger.debug("base="+base);
      logger.debug(this.getClass().getName());
    }    
    String dir = this.getClass().getName().replaceAll("\\.","/")+"/";   
    
    String result = base+dir+file;
    if(logger.isDebugEnabled()) {
      logger.debug("base="+base+", dir="+dir);
      logger.debug("result="+result);
    }
    try {
      return new File(new URI(result)).getPath();
    } catch (URISyntaxException e) {
      logger.debug("Bad URI: ",e);
    }
    return result;
  }
  
  public void input(Graph g, String file)  throws IOException, MalformatedException {
    input(null,g,file);
  }  
  public void input(GraphFileReader gfr,Graph g,String file) throws IOException, MalformatedException {
    file = this.getTestFile(file);
    gfr = new DirectedUndirectedToUndirected(file,g);
    gfr.read();
  }
}
