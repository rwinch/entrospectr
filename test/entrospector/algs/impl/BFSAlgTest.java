/*
 * 
 */
package entrospector.algs.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import entrospector.algs.impl.BFSAlg;
import entrospector.graph.impl.AbstractGraphTest;
import entrospector.graph.impl.UndirectedEdgeImpl;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;

/**
 * @author rwinch
 */
public class BFSAlgTest extends AbstractGraphTest {
  private static Logger logger = Logger.getLogger(BFSAlgTest.class);
  
  private BFSAlg bfs;
  
  public void setUp() {    
    super.setUp();
    bfs = new BFSAlg();
    
    //  init the graph
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(ab);
    edgesToAdd.add(ac);
    edgesToAdd.add(bd);
    edgesToAdd.add(cd);
    
    edgesToAdd.add(eg);
    edgesToAdd.add(fe);
    edgesToAdd.add(fi);
    edgesToAdd.add(gg);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
  }
  
  public void tearDown() {
    super.tearDown();
    bfs = null;
  }
  public void testEdgeVertex() {
    logger.info("testEdgeVertex()");
    
    graph = new UndirectedGraphImpl();
    
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
    
    graph.addEdge(ac);
    graph.addEdge(new UndirectedEdgeImpl(a,c));    
    graph.addEdge(ab);
    graph.addEdge(bb);
    graph.addEdge(bd);
    graph.addEdge(cd);
    graph.addEdge(de);
    
    HashMap args = new HashMap();
    args.put("graph",graph);
    args.put("source",a);
    
    Graph g = (Graph) bfs.run(args);
    
    assertEquals(7,g.getEdges().size());
    if(logger.isDebugEnabled()) {
      logger.debug(g.getEdges());
    }
  }
  public void testWithSource() {
    HashMap args = new HashMap();
    args.put("graph",graph);
    args.put("source",a);
    
    Graph result = (Graph)bfs.run(args);
    
    Vertex A = result.getVertex(a);
    Vertex B = result.getVertex(b);
    Vertex C = result.getVertex(c);
    Vertex D = result.getVertex(d);
    
    BFSAlg.DataImpl aData = (BFSAlg.DataImpl) result.getDataFor(A).get(0);
    BFSAlg.DataImpl bData = (BFSAlg.DataImpl) result.getDataFor(B).get(0);
    BFSAlg.DataImpl cData = (BFSAlg.DataImpl) result.getDataFor(C).get(0);
    BFSAlg.DataImpl dData = (BFSAlg.DataImpl) result.getDataFor(D).get(0);
    
    assertEquals(a,aData.getVertex());
    assertNull(aData.getParent());
    assertEquals(0,aData.getDistance());
    assertEquals(BFSAlg.DataImpl.BLACK,aData.getColor());
    
    assertEquals(b,bData.getVertex());
    assertEquals(A,bData.getParent());
    assertEquals(1,bData.getDistance());
    assertEquals(BFSAlg.DataImpl.BLACK,bData.getColor());
    
    assertEquals(c,cData.getVertex());
    assertEquals(A,cData.getParent());
    assertEquals(1,cData.getDistance());
    assertEquals(BFSAlg.DataImpl.BLACK,cData.getColor());
        
    assertEquals(d,dData.getVertex());
    assertTrue(dData.getParent().equals(B) || dData.getParent().equals(C));
    assertEquals(2,dData.getDistance());
    assertEquals(BFSAlg.DataImpl.BLACK,dData.getColor());
  }
  
  public void testNoSource() {
    logger.info("testNoSource()");
    
    HashMap args = new HashMap();
    args.put("graph",graph);
    
    Collection graphs = (Collection)bfs.run(args);
    assertEquals(2,graphs.size());
    
    // TODO: add more tests
    
    
  }
}
