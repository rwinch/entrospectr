/*
 * 
 */
package entrospector.algs.impl;

import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.AbstractEntrospectorTest;
import entrospector.Utilities;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Graph;
import entrospector.io.interfaces.GraphFileReader;


/**
 * @author rwinch
 */
public class StatsAlgTest extends AbstractEntrospectorTest {
  private static final Logger logger = Logger.getLogger(StatsAlgTest.class);
  
  private Graph g1,g2;
  private GraphFileReader gfr1,gfr2; 
  
  public void setUp() {
    g1 = new UndirectedGraphImpl();
    g2 = new UndirectedGraphImpl();    
  }
  public void tearDown() {
    g1 = g2 = null;
  }
  public void testVerticesHStatChanged() throws Exception {
    input(gfr1,g1,"verticesChanged1");
    input(gfr2,g2,"verticesChanged2");
    
    Map origionalHStat = StatsAlg.getOrigionalHStat(g1);
    if(logger.isDebugEnabled()) {
      logger.debug("orgigionalHStat="+origionalHStat);
    }
  	StatsAlg.Result statsAlg = StatsAlg.go(
  	    Utilities.arrayToMap(
  	        new Object[] { 
  	        "origionalHStat", origionalHStat,
            "origionalGraph", g1,
            "newGraph", g2 }));
  	
  	assertEquals(2,statsAlg.getVDeltaHStatCnt());
  	assertEquals(.5,statsAlg.getVDeltaHStatPercent(),PRECISSION);
  }    
}
