/*
 * 
 */
package entrospector.algs.impl;

import entrospector.AbstractEntrospectorTest;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Graph;

/**
 * @author rwinch
 */
public abstract class AbstractDeletionsTest extends AbstractEntrospectorTest {
  /**
   * The vertices in this array will provide the vertices and the order for vertices
   * to be removed
   */
  protected Object[] verticesToDelete;  
    
  /**
   * A modified version of the alg that returns specific vertices to delete. We test the
   * randomoness separately in Utilities test. This allows us to know which vertices
   * are removed.
   */
  protected RandomDeletionsAlg randDelAlg = new RandomDeletionsAlg() {
    /**
     * Override one method allowing us to know what vertices are removed
     */    
    public Object[] getVerticies(Graph graph) {
      return verticesToDelete;
    }
  };
  /**
   * Provide two graphs
   */
  protected Graph g1 = new UndirectedGraphImpl(),
  	g2 = new UndirectedGraphImpl();
  
  public void tearDown() {
    verticesToDelete = null;
  }
}
