/*
 * 
 */
package entrospector.algs.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import entrospector.AbstractEntrospectorTest;
import entrospector.graph.impl.UndirectedEdgeImpl;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Graph;
import entrospector.io.impl.DirectedUndirectedToUndirected;
import entrospector.io.impl.MalformatedException;
import entrospector.io.interfaces.GraphFileReader;

/**
 * @author rwinch
 */
public class HFlatAlgTest extends AbstractEntrospectorTest {
  private static Logger logger = Logger.getLogger(HFlatAlgTest.class);
  
  private Graph graph;
  private GraphFileReader gfr;
  private Double algResult;
  private HFlatAlg hFlat;
  public static final String FILE_PREFIX = "HFlat_";
  
  private final static double PRECISSION = 0.0000001;
  
  public void setUp() {
    graph = new UndirectedGraphImpl();
    hFlat = new HFlatAlg();
  }
  
  public void tearDown() {
    graph = null;
    hFlat = null;
  }
  public void readGraph(String file) throws IOException, MalformatedException {
    gfr = new DirectedUndirectedToUndirected(getTestFile(file),graph);
    gfr.read();    
  }

  
  public void testGetFilteredGraph() throws Exception {
    readGraph("GetFilteredGraph.txt");
    Graph result = new UndirectedGraphImpl();
    result.addGraphs(hFlat.filterGraph(graph).getRetainedGraphs());
    
    // check if the correct edges are in result
    Collection expctdRetainedEdges = new LinkedList();
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("1","1"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("1","2"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("1","3"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("1","2"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("1","3"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("1","4"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("1","5"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("2","2"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("2","3"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("3","6"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("4","5"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("4","8"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("5","5"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("6","7"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("6","9"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("8","8"));
    
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("30","31"));
    expctdRetainedEdges.add(UndirectedEdgeImpl.create("31","32"));
    

    assertTrue(result.getEdges().containsAll(expctdRetainedEdges));
    
    Collection expctdRemovedEdges = new LinkedList();
    expctdRemovedEdges.add(UndirectedEdgeImpl.create("10","10"));
    expctdRemovedEdges.add(UndirectedEdgeImpl.create("20","21"));
    
    // should contain none
    assertTrue(!result.getEdges().containsAll(expctdRemovedEdges)); 
    
    // ensure origional graph is the same
    assertTrue(graph.getEdges().containsAll(expctdRetainedEdges));
    assertTrue(graph.getEdges().containsAll(expctdRemovedEdges));
  }
  public void testGetFilteredGraphValues() throws Exception {
    logger.info("testGraphGeneralHStat()");
    readGraph("GetFilteredGraph.txt");
    
    HFlatAlg.Result result = (HFlatAlg.Result) hFlat.run(Collections.singletonMap("graph",graph));
    if(logger.isDebugEnabled()) {
      logger.debug("result="+result.getHStat());
    }
    
    assertEquals(14.509774746352964,result.getHStat().doubleValue(),PRECISSION);
    assertEquals(16.43080571598863,result.getHFlat().doubleValue(),0);
  }
  
  public void testValues(String file, double hStatResult,double hFlatResult, double precission) throws Exception {
    readGraph(file);
    
    HFlatAlg.Result result = (HFlatAlg.Result) hFlat.run(Collections.singletonMap("graph",graph));
    
    if(logger.isDebugEnabled()) {
      logger.debug("result.hStat="+result.getHStat()+", result.hFlat="+result.getHFlat());
    }
    
    assertEquals(hFlatResult,result.getHFlat().doubleValue(),precission);
    assertEquals(hStatResult,result.getHStat().doubleValue(),precission);
  }
  
  public void testHaught1() throws Exception {
    logger.info("testHaught1");    
    testValues("HaughtEx1.txt",7.169925001,8.348515546,PRECISSION);    
  }
  
  public void testHaught2()  throws Exception {
    logger.info("testHaught2");    
    testValues("HaughtEx2.txt",7.169925001,8.348515546,PRECISSION);
  }
  
  public void testHaught3()  throws Exception {
    logger.info("testHaught3");    
    testValues("HaughtEx3.txt",8.169925001,9.702022608,PRECISSION);
  }
  
  public void testHaught4()  throws Exception {
    logger.info("testHaught4");    
    testValues("HaughtEx4.txt",7.169925001,8.348515546,PRECISSION);
  }
  
  public void testHaught5()  throws Exception {
    logger.info("testHaught5");    
    testValues("HaughtEx5.txt",7.169925001,8.348515546,PRECISSION);
  }
  public void testHaught6()  throws Exception {
    logger.info("testHaught6");    
    testValues("HaughtEx6.txt",3,3.390359526,PRECISSION);
  }
  public void testHaught7()  throws Exception {
    logger.info("testHaught8");    
    testValues("HaughtEx7.txt",5,5,PRECISSION);
  }
  public void testHaught8()  throws Exception {
    logger.info("testHaught8");    
    testValues("HaughtEx8.txt",8,8.390359526,PRECISSION);
  }
  
  public void testHaught9()  throws Exception {
    logger.info("testHaught9");    
    testValues("HaughtEx9.txt",3,7.470674987,PRECISSION);
  }
  
  public void testHaught10()  throws Exception {
    logger.info("testHaught10");    
    testValues("HaughtEx10.txt",6,14.94134997,PRECISSION);
  }
  
  public void testHaught11()  throws Exception {
    logger.info("testHaught11");    
    testValues("HaughtEx11.txt",6.339850003,16.51568112,PRECISSION);
  }
  
  public void testHaught12()  throws Exception {
    logger.info("testHaught12");    
    testValues("HaughtEx12.txt",22.33985,26.53076139,PRECISSION);
  }
  
  

}
