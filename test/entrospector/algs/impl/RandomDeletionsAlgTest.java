/*
 *
 */
package entrospector.algs.impl;

import java.util.Collection;
import java.util.LinkedList;

import entrospector.Utilities;
import entrospector.graph.interfaces.Graph;

/**
 * @author rwinch
 */
public class RandomDeletionsAlgTest extends AbstractDeletionsTest {
  
  
  public void testVerticiesRetained() throws Exception {
    this.input(g1,"verticiesRetained1");
    
    verticesToDelete = new Object[] {
        _1,_2,_3,_8
    };
    
    RandomDeletionsAlg.Result result = 
        (RandomDeletionsAlg.Result) randDelAlg.run(
        Utilities.arrayToMap(
            new Object[] {
               "deletionCount",new Integer(4),
               "graph",g1
            }
        )
    );
    
    Collection expectedRetained = new LinkedList();
    expectedRetained.add(_4);
    expectedRetained.add(_5);
    expectedRetained.add(_6);
    expectedRetained.add(_7);
    
    expectedRetained.add(_9);
    expectedRetained.add(_10);
    
    Graph graphResult = result.getGraph();    
    
    assertTrue(graphResult.getVertices().containsAll(expectedRetained));
    assertTrue(expectedRetained.containsAll(graphResult.getVertices()));
  } 
 
}
