/*
 * 
 */
package entrospector.algs.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.AbstractEntrospectorTest;
import entrospector.Utilities;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.impl.VertexImpl;
import entrospector.graph.interfaces.Graph;

/**
 * @author rwinch
 */
public class DuplicationAlgTest extends AbstractEntrospectorTest {
  private static final Logger logger = Logger.getLogger(DuplicationAlgTest.class);
  
  protected Object[] verticesToDuplicate;
  protected Graph g = new UndirectedGraphImpl();
  
  DuplicationAlg dupAlg = new DuplicationAlg() {
    public Object[] getVertices(Graph g) {
      return verticesToDuplicate;
    }
  };
  
  
  public void testOrgToDuplicated() throws Exception {
    input(g,"orgToDuplicated.txt");
    
    verticesToDuplicate = new Object[] {
     _3,_4,_10
    };
    Collection expectedOrg = new LinkedList(Arrays.asList(verticesToDuplicate));
    Collection expectedDup = new LinkedList(
        Arrays.asList(
            new Object[] {
                VertexImpl.create("11"),
                VertexImpl.create("12"),
                VertexImpl.create("13")
            }
        )
    );
    
    DuplicationAlg.Result result = (DuplicationAlg.Result)dupAlg.run(
        Utilities.arrayToMap(
            new Object[] {
                "graph",g,
                "duplicationCount",new Integer(verticesToDuplicate.length)
            }
        )
    );
    Map orgToDup = result.getOrigionalToDuplicated();
    
    // check the orgional
    assertTrue(orgToDup.keySet().containsAll(expectedOrg));
    assertTrue(expectedOrg.containsAll(orgToDup.keySet()));
    
    // check the duplicated
    assertTrue(orgToDup.values().containsAll(expectedDup));
    assertTrue(expectedDup.containsAll(orgToDup.values()));    
  }
  
  public void testVerticesInNewGraph() throws Exception {
    input(g,"orgToDuplicated.txt");
    
    verticesToDuplicate = new Object[] {
     _3,_4,_10
    };
    Collection expectedOrg = new LinkedList(Arrays.asList(verticesToDuplicate));
    Collection expectedDup = new LinkedList(
        Arrays.asList(
            new Object[] {
                VertexImpl.create("11"),
                VertexImpl.create("12"),
                VertexImpl.create("13")
            }
        )
    );
    
    DuplicationAlg.Result result = (DuplicationAlg.Result)dupAlg.run(
        Utilities.arrayToMap(
            new Object[] {
                "graph",g,
                "duplicationCount",new Integer(verticesToDuplicate.length)
            }
        )
    );
    // check the new nodes are in    
    assertTrue(result.getGraph().getVertices().containsAll(expectedDup));
    // check the old nodes are in
    assertTrue(result.getGraph().getVertices().containsAll(g.getVertices()));
  }  
}
