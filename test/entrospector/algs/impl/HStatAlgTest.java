/*
 * 
 */
package entrospector.algs.impl;

import java.io.IOException;
import java.util.Collections;

import org.apache.log4j.Logger;

import entrospector.AbstractEntrospectorTest;
import entrospector.algs.interfaces.Algorithm;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;
import entrospector.io.impl.DirectedUndirectedToUndirected;
import entrospector.io.impl.MalformatedException;
import entrospector.io.interfaces.GraphFileReader;

/**
 * @author rwinch
 */
public class HStatAlgTest extends AbstractEntrospectorTest {
  private static Logger logger = Logger.getLogger(HStatAlgTest.class);
  
  private Graph graph;
  private GraphFileReader gfr;
  private Double algResult;
  private Algorithm hStat;
  public static final String FILE_PREFIX = "HSTAT_";

  private final static double PRECISSION = 0.0000001;
  
  public void setUp() {
    graph = new UndirectedGraphImpl();
    hStat = new HStatAlg();
  }
  
  public void tearDown() {
    graph = null;
    hStat = null;
  }
  public void readGraph(String file) throws IOException, MalformatedException {
    gfr = new DirectedUndirectedToUndirected(getTestFile(file),graph);
    gfr.read();    
  }
  private double getHStat(String vId) {
    Vertex v = graph.getVertex(vId);
    algResult = HStatAlg.getHStatForVertexIn(v,graph).getHStat();
    if(logger.isDebugEnabled()) {
      logger.debug("hStat["+v+"]="+algResult.doubleValue());
    }
    return algResult.doubleValue();
  }
  public void testGetVertexHStat() throws Exception {
    logger.info("testVertexHStat()");
    readGraph("VertexHStat.txt");
    // TODO: add checks
    // need to actually compute these values and ensure they are right
    // so far only checked to see if the values that are used to compute
    // answer are correct, but formual should be correct
  }
  
  public void testGraphHStat() throws Exception {
    logger.info("testGraphHStat()");
    readGraph("GraphHStat.txt");
    HStatAlg.Result result = (HStatAlg.Result) hStat.run(Collections.singletonMap("graph",graph));
    if(logger.isDebugEnabled()) {
      logger.debug("result="+result.getHStat());
    }
    assertEquals(4.584962457725494,result.getHStat().doubleValue(),PRECISSION);
  }
  
  public void testGeneralHStat() throws Exception {
    logger.info("testGraphGeneralHStat()");
    readGraph("GeneralHStat.txt");
    
    HStatAlg.Result result = (HStatAlg.Result) hStat.run(Collections.singletonMap("graph",graph));
    if(logger.isDebugEnabled()) {
      logger.debug("result="+result.getHStat());
    }
    assertEquals(3.584962457725494,result.getHStat().doubleValue(),PRECISSION);
  }

}
