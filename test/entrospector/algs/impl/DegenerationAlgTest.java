/*
 * 
 */
package entrospector.algs.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.AbstractEntrospectorTest;
import entrospector.Utilities;
import entrospector.graph.impl.UndirectedEdgeImpl;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.impl.VertexImpl;
import entrospector.graph.interfaces.Edge;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;

/**
 * @author rwinch
 */
public class DegenerationAlgTest extends AbstractEntrospectorTest {
  private static final Logger logger = Logger.getLogger(DegenerationAlgTest.class);
  
  private Graph graph = new UndirectedGraphImpl();
  private int choiceIndex = 0;
  private int[] choiceValues = new int[] {1,0,1,1,3,3,3};
    
  protected DegenerationAlg degenAlg = new DegenerationAlg() {
    public int nextChoice(int choiceCnt) {
      int result = choiceValues[choiceIndex] % choiceCnt;
      choiceIndex += 1; choiceIndex = choiceIndex % choiceValues.length;
      countCnt[result]++;
      return result;
    }
  };
  
  protected DegenerationAlg.Result degenResult;
    
  public void setUp() throws Exception {
    input(this.graph,"resultGraph.txt");
    
    Vertex _11 = VertexImpl.create("11"),_12 = VertexImpl.create("12"), _13 =VertexImpl.create("13");
    // these would be added by Duplication alg
    graph.addVertices(Arrays.asList(new Object[] {_11,_12,_13}));
    
    Map origionalToDuplicated = 
      Utilities.arrayToMap(new Object[] {
        _3,_11,
        _4,_12,
        _10,_13
      }
    );
    
    degenResult = 
      (DegenerationAlg.Result) degenAlg.run(
          Utilities.arrayToMap(
              new Object[] {
                  DegenerationAlg.PARAM_graph,graph,
                  DegenerationAlg.PARAM_origionalToDuplicated,origionalToDuplicated
              }
          )
      );
  }
  public void testRedundantEdges() throws Exception {
    // check redundant edges
    Collection exptRdntEdges = Arrays.asList(
        new Object[] {
            UndirectedEdgeImpl.create("2","3"),
            UndirectedEdgeImpl.create("5","12"),
            UndirectedEdgeImpl.create("6","4"),
            UndirectedEdgeImpl.create("7","3"),
            UndirectedEdgeImpl.create("10","13"),
            UndirectedEdgeImpl.create("13","13")
        }
    );    
    assertTrue(exptRdntEdges.containsAll(degenResult.getRedundantEdges()));
    assertTrue(degenResult.getRedundantEdges().containsAll(exptRdntEdges));
  }
  public void testConnectedEdges() throws Exception {
    // check the connected edges
    Collection exptNewGraphEdges = Arrays.asList(
        new Object[] {
            UndirectedEdgeImpl.create("2","11"),
            UndirectedEdgeImpl.create("7","11"),
            UndirectedEdgeImpl.create("4","5"),
            UndirectedEdgeImpl.create("12","6")
        }
    );
    assertTrue(degenResult.getGraph().getEdges().containsAll(exptNewGraphEdges));
  }
  public void testRemovedEges() throws Exception {
    // check the removed edges
    Edge[] exptRmvdGraphEdges = new Edge[] {
      UndirectedEdgeImpl.create("2","3"),
      UndirectedEdgeImpl.create("7","3"),
      UndirectedEdgeImpl.create("4","6")
    };
    
    for(int i=0;i<exptRmvdGraphEdges.length;i++) {
      Edge e = exptRmvdGraphEdges[i];
      assertTrue(!degenResult.getGraph().getEdges().contains(e));
    }
    
  }
}
