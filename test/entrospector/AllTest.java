/*
 * 
 */
package entrospector;

import java.lang.reflect.Modifier;
import java.util.Enumeration;

import org.apache.log4j.Logger;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.runner.ClassPathTestCollector;
import junit.runner.LoadingTestCollector;

/**
 * @author rwinch
 */
public class AllTest {
  private static final Logger logger = Logger.getLogger(AllTest.class);

  public static Test suite() throws Exception {
    TestSuite suite = new TestSuite("TEST ALL");
    // get all of the classes on the class path
    ClassPathTestCollector c = new LoadingTestCollector();
    Enumeration tests = c.collectTests();
    logger.debug("gathering tests");
    while (tests.hasMoreElements()) {
      // for each className
      String className = (String) tests.nextElement();
      logger.debug("checking className="+className);
      try {
        // if not ourself
        if (!AllTest.class.getName().equals(className)) {
          Class test = Class.forName(className);
          // ensure that it is not abstract (can't test abstract classes)
          if(!Modifier.isAbstract(test.getModifiers())) {
            logger.debug("adding");
            suite.addTestSuite(test);
          }
        }
      } catch (Exception e) {
        logger.error("Couldn't add " + className + " to test suite", e);
      }
    }
    return suite;
  }
}