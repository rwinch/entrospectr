/*
 * 
 */
package entrospector.graph.impl;

import org.apache.log4j.Logger;

import entrospector.graph.interfaces.Edge;
import entrospector.graph.impl.UndirectedEdgeImpl;
import entrospector.graph.impl.VertexImpl;
import entrospector.graph.interfaces.Vertex;
import junit.framework.TestCase;

/**
 * @author rwinch
 */
public class UndirectedEdgeTest extends TestCase {
  private static Logger logger = Logger.getLogger(UndirectedEdgeImpl.class);
  
  private Edge aa, aa2, ab, ba, ac, a2c, ca;
  
  public void setUp() {
    Vertex a = VertexImpl.create("a");
    Vertex a2 = VertexImpl.create("a");
    Vertex b = VertexImpl.create("b");
    Vertex c = VertexImpl.create("c");
    
    
    aa = new UndirectedEdgeImpl(a,a);
    aa2 = new UndirectedEdgeImpl(a,a2);
    ab =  new UndirectedEdgeImpl(a,b);
    ba = new UndirectedEdgeImpl(b,a);
    ac = new UndirectedEdgeImpl(a,c);
    a2c = new UndirectedEdgeImpl(a2,c);
    ca = new UndirectedEdgeImpl(c,a);
  }
  
  public void tearDown() {
    aa = aa2 = ab = ba = ac = a2c = ca = null;
  }
  
  public void testCompareSame() {
    logger.info("testCompareSame()");
    assertSame(aa,aa);
    assertEquals(aa,aa);
    assertEquals(aa,aa2);
  }
  
  public void testCompareCompliment() {
    logger.info("testCompareCompliment()");
    assertEquals(ab,ba);
    assertEquals(ac,a2c);
    assertEquals(a2c,ca);
    assertEquals(ac,ca);
  }
  
  public void testCompareDifferent() {
    logger.info("testCompareDifferent()");
    assertTrue(aa.compareTo(ab) < 0);
    assertTrue(aa.compareTo(ba) < 0);
    assertTrue(aa.compareTo(ac) < 0);
    assertTrue(aa.compareTo(a2c) < 0);
    assertTrue(aa.compareTo(ca) < 0);
  }
}
