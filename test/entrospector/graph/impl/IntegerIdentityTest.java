/*
 * 
 */
package entrospector.graph.impl;

import org.apache.log4j.Logger;

import junit.framework.TestCase;
import entrospector.graph.interfaces.Identity;

/**
 * @author rwinch
 */
public class IntegerIdentityTest extends TestCase {
  private static final Logger logger = Logger.getLogger(IntegerIdentityTest.class);
  
  private Identity _n45, _0, _1, _34, _149, _3410;
  private Identity _sn45, _s0, _s1, _s34, _s149, _s3410;
  
  public void setUp() {
    _n45 = new IntegerIdentity(-45);
    _0 = new IntegerIdentity(0);
    _1 = new IntegerIdentity(1);
    _34 = new IntegerIdentity(34);
    _149 = new IntegerIdentity(149);
    _3410 = new IntegerIdentity(3410);
    
    _sn45 = new IntegerIdentity("-45");
    _s0 = new IntegerIdentity("0");
    _s1 = new IntegerIdentity("1");
    _s34 = new IntegerIdentity("34");
    _s149 = new IntegerIdentity("149");
    _s3410 = new IntegerIdentity("3410");
  }
  public void tearDown() {
    _n45 = _0 = _1 = _34 = _149 = _3410 = null;
    _sn45 = _s0 = _s1 = _s34 = _s149 = _s3410 = null;
  }

  public void testEqual() {
    assertEquals(_n45,_sn45);
    assertEquals(_0,_s0);
    assertEquals(_1,_s1);
    assertEquals(_34,_s34);
    assertEquals(_149,_s149);
    assertEquals(_3410,_s3410);
    
    assertEquals(_3410,new Integer(3410));
  }
  
  
  public boolean isAGreaterThanB(Comparable a, Comparable b) {    
    int result = a.compareTo(b);
    if(logger.isDebugEnabled()) {
      logger.debug("isGreater("+a+","+b+")");
      logger.debug("result="+result);
    }
    return result > 0;
  }
  public boolean isBLessThanA(Comparable a, Comparable b) {
    return b.compareTo(a) < 0;
  }
  
  public void testInvalid() {
    String[] invalid = new String[] {"abc","--123","1.23"};
    
    for(int i=0;invalid!=null && i<invalid.length;i++) {
      try {
        new IntegerIdentity(invalid[i]);
        fail("Exected invalid identity");
      }catch(Exception e) {
        // success
      }
    }
  }  
  public void testGreaterInt() {
    assertTrue(isAGreaterThanB(_0,_n45));
    assertTrue(isAGreaterThanB(_34,_n45));
    assertTrue(isAGreaterThanB(_34,_0));
    assertTrue(isAGreaterThanB(_149,_1));
    assertTrue(isAGreaterThanB(_3410,_n45));    
  }
  public void testGreaterStr() {
    assertTrue(isAGreaterThanB(_s0,_sn45));
    assertTrue(isAGreaterThanB(_s34,_sn45));
    assertTrue(isAGreaterThanB(_s34,_s0));
    assertTrue(isAGreaterThanB(_s149,_s1));
    assertTrue(isAGreaterThanB(_s3410,_sn45));    
  }
  public void testGreaterIntStr() {
    assertTrue(isAGreaterThanB(_0,_sn45));
    assertTrue(isAGreaterThanB(_s34,_n45));
    assertTrue(isAGreaterThanB(_34,_s0));
    assertTrue(isAGreaterThanB(_149,_s1));
    assertTrue(isAGreaterThanB(_3410,_sn45));    
  }

  public void testLessInt() {
    assertTrue(isBLessThanA(_0,_n45));
    assertTrue(isBLessThanA(_34,_n45));
    assertTrue(isBLessThanA(_34,_0));
    assertTrue(isBLessThanA(_149,_1));
    assertTrue(isBLessThanA(_3410,_n45));    
  }
  public void testLessStr() {
    assertTrue(isBLessThanA(_s0,_sn45));
    assertTrue(isBLessThanA(_s34,_sn45));
    assertTrue(isBLessThanA(_s34,_s0));
    assertTrue(isBLessThanA(_s149,_s1));
    assertTrue(isBLessThanA(_s3410,_sn45));    
  }
  public void testLessIntStr() {
    assertTrue(isBLessThanA(_0,_sn45));
    assertTrue(isBLessThanA(_s34,_n45));
    assertTrue(isBLessThanA(_34,_s0));
    assertTrue(isBLessThanA(_149,_s1));
    assertTrue(isBLessThanA(_3410,_sn45));    
  }
  
  public void testNext() {
    assertEquals(new IntegerIdentity(-44),_n45.next());
    assertEquals(new IntegerIdentity(1),_0.next());
    assertEquals(new IntegerIdentity(35),_34.next());
    assertEquals(new IntegerIdentity(150),_149.next());
    assertEquals(new IntegerIdentity(3411),_3410.next());
  }
  
}
