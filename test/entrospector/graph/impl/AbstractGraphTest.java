/*
 * Created on Feb 20, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package entrospector.graph.impl;

import entrospector.AbstractEntrospectorTest;
import entrospector.graph.impl.UndirectedEdgeImpl;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.impl.VertexImpl;
import entrospector.graph.interfaces.Edge;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;

/**
 * @author rwinch
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public abstract class AbstractGraphTest extends AbstractEntrospectorTest {
  
  protected Graph graph;

  protected Vertex a, a2, b, c, d, e, f, g, h, i;

  protected Edge aa, aa2, ab, ac, ad, ah, ai, a2b, bb, bd, be, bh, cd, cf, de, dg, di, ea, ec,
      eg, fa, fe, fi, gg, hb, hi, ie, ii;

  public void setUp() {
    // the graph
    graph = new UndirectedGraphImpl();

    // vertices
    a = VertexImpl.create("a");
    a2 = VertexImpl.create("a");
    b = VertexImpl.create("b");
    c = VertexImpl.create("c");
    d = VertexImpl.create("d");
    e = VertexImpl.create("e");
    f = VertexImpl.create("f");
    g = VertexImpl.create("g");
    h = VertexImpl.create("h");
    i = VertexImpl.create("i");

    // Edges
    aa = new UndirectedEdgeImpl(a, a);
    aa2 = new UndirectedEdgeImpl(a, a2);
    ab = new UndirectedEdgeImpl(a, b);
    ac = new UndirectedEdgeImpl(a, c);
    ad = new UndirectedEdgeImpl(a, d);
    ah = new UndirectedEdgeImpl(a, h);
    ai = new UndirectedEdgeImpl(a, i);
    a2b = new UndirectedEdgeImpl(a2, b);
    bb = new UndirectedEdgeImpl(b, b);
    bd = new UndirectedEdgeImpl(b, d);
    be = new UndirectedEdgeImpl(b, e);
    bh = new UndirectedEdgeImpl(b, h);
    cd = new UndirectedEdgeImpl(c, d);
    cf = new UndirectedEdgeImpl(c, f);
    de = new UndirectedEdgeImpl(d, e);
    dg = new UndirectedEdgeImpl(d, g);
    di = new UndirectedEdgeImpl(d, i);
    ea = new UndirectedEdgeImpl(e, a);
    ec = new UndirectedEdgeImpl(e, c);
    eg = new UndirectedEdgeImpl(e, g);
    fa = new UndirectedEdgeImpl(f, a);
    fe = new UndirectedEdgeImpl(f, e);
    fi = new UndirectedEdgeImpl(f, i);
    gg = new UndirectedEdgeImpl(g, g);
    hb = new UndirectedEdgeImpl(h, b);
    hi = new UndirectedEdgeImpl(h, i);
    ie = new UndirectedEdgeImpl(i, e);
    ii = new UndirectedEdgeImpl(i, i);
  }

  public void tearDown() {
    a = a2 = b = c = d = e = f = g = h = i = null;
    aa = aa2 = ab = ac = ad = ah = ai = a2b = bb = bd = be = bh = cd = cf = de = dg = di = ea = ec = eg = fa = fe = fi = gg = hb = hi = ie = ii = null;
  }
}
