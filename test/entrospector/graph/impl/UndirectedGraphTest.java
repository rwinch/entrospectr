/*
 *
 */
package entrospector.graph.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;


/**
 * @author rwinch
 */
public class UndirectedGraphTest extends AbstractGraphTest {
  private Logger logger = Logger.getLogger(UndirectedGraphTest.class);

  // modifiers
  public void testAddEdge() {
    logger.info("testAddEdge()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());

    // add an edge see if the vertices appear
    graph.addEdge(aa);    
    assertEquals(1,graph.getEdges().size()); // should add one edge
    assertEquals(1,graph.getVertices().size()); // should add a vertex (not both)
    graph.addEdge(aa2); // a "new" duplicate edge, equal but not ==
    assertEquals(2, graph.getEdges().size()); // should add edge
    assertTrue(graph.getVertices().size() == 1); // shouldn't add any vertices
    graph.addEdge(be);
    assertTrue(graph.getEdges().size() == 3); // should add an edge
    assertTrue(graph.getVertices().size() == 3); // should add two new Vertex
  }
  
  public void testAddEdges() {
    logger.info("testAddEdges()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());

    Collection edges = new LinkedList();
    edges.add(aa);
    edges.add(aa2);
    edges.add(be);
    graph.addEdges(edges);
    assertTrue(graph.getEdges().size() == 3);
    assertTrue(graph.getVertices().size() == 3);
  }

  public void testAddGraph() {
    logger.info("testAddGraph()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());

    Graph graph2 = new UndirectedGraphImpl();
    Collection edges = new LinkedList();
    edges.add(aa);
    edges.add(aa2);
    edges.add(be);
    graph2.addEdges(edges);

    graph.addGraph(graph2);
    assertTrue(graph.getEdges().size() == 3);
    assertTrue(graph.getVertices().size() == 3);
  }

  public void testAddVertex() {
    logger.info("testAddVertex()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());

    assertTrue(graph.addVertex(a));
    assertEquals(1, graph.getVertices().size());
    assertFalse(graph.addVertex(a2));
    assertEquals(1, graph.getVertices().size());
    assertTrue(graph.addVertex(b));
    assertEquals(2, graph.getVertices().size());
    assertTrue(graph.addVertex(c));
    assertEquals(3, graph.getVertices().size());

    assertTrue(graph.getEdges().isEmpty());
  }

  public void testAddVertices() {
    logger.info("testAddVertices()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());

    Collection vertices = new LinkedList();
    vertices.add(a);
    vertices.add(a2);
    vertices.add(b);
    vertices.add(c);
    vertices.add(c);
    vertices.add(d);
    vertices.add(e);

    graph.addVertices(vertices);
    assertTrue(graph.getEdges().isEmpty());
    assertEquals(5, graph.getVertices().size());
  }

  public void testRemoveEdge() {
    logger.info("testRemoveEdge()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());

    Collection edges = new LinkedList();
    edges.add(aa);
    edges.add(aa2);
    edges.add(be);
    edges.add(cd);
    edges.add(ii);

    graph.addEdges(edges);

    // check initial state
    assertEquals(5, graph.getEdges().size());
    assertEquals(6, graph.getVertices().size());

    // remove an edge that doesn't exist, should be false    
    assertFalse(graph.removeEdge(dg));
    assertEquals(5, graph.getEdges().size());
    assertEquals(6, graph.getVertices().size());

    // remove an edge that does exist (should be true)
    assertTrue(graph.removeEdge(aa));
    assertEquals(4, graph.getEdges().size());
    assertEquals(6, graph.getVertices().size());

    // remove an edge aa2 by using aa
    assertTrue(graph.removeEdge(aa));
    assertEquals(3, graph.getEdges().size());
    assertEquals(6, graph.getVertices().size());

    // check c & d's edges
    if(logger.isDebugEnabled()) {
      logger.debug("graph.getEdgesOf(c)"+ graph.getEdgesOf(c));
    }
    assertEquals(1, graph.getEdgesOf(c).size());
    assertEquals(1, graph.getEdgesOf(d).size());
    // remove an edge (should work)
    assertTrue(graph.removeEdge(cd));
    assertEquals(2, graph.getEdges().size());
    assertEquals(6, graph.getVertices().size());
    // check c & d's edges
    assertEquals(0, graph.getEdgesOf(c).size());
    assertEquals(0, graph.getEdgesOf(d).size());
    
    // remove edge eb
    if(logger.isDebugEnabled()) {
	    logger.debug("Edges of e before remove(e,b): "+graph.getEdgesOf(e));
	    logger.debug("Edges of b before remove(e,b): "+graph.getEdgesOf(b));
	    logger.debug("removing edge...");
    }
    assertTrue(graph.removeEdge(new UndirectedEdgeImpl(e,b)));
    if(logger.isDebugEnabled()) {
	    logger.debug("Edges of e after remove(e,b): "+graph.getEdgesOf(e));
	    logger.debug("Edges of b after remove(e,b): "+graph.getEdgesOf(b));
    }
  }

  public void testRemoveEdges() {
    logger.info("testRemoveEdges()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());

    // init the graph
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hb);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);

    // ensure they added correctly
    assertEquals(11, graph.getEdges().size());
    assertEquals(9, graph.getVertices().size());

    // init edgesToRemove
    Collection edgesToRemove = new LinkedList();
    edgesToRemove.add(cf); // will fail
    edgesToRemove.add(aa);
    edgesToRemove.add(aa); // removes aa2
    edgesToRemove.add(di);
    edgesToRemove.add(di); // will fail

    graph.removeEdges(edgesToRemove);
    assertEquals(8, graph.getEdges().size());
    assertEquals(9, graph.getVertices().size());

    graph.removeEdges(edgesToAdd);
    assertEquals(0, graph.getEdges().size());
    assertEquals(9, graph.getVertices().size());
  }

  public void testRemoveGraph() {
    logger.info("testRemoveGraph()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());

  }

  public void testRemoveVertex() {
    logger.info("testRemoveVertex()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());

    Collection vertices = new LinkedList();
    vertices.add(a);
    vertices.add(a2);
    vertices.add(b);
    vertices.add(c);
    vertices.add(c);
    vertices.add(d);
    vertices.add(e);
    vertices.add(g);
    vertices.add(i);
    graph.addVertices(vertices);

    assertEquals(7, graph.getVertices().size());
    assertEquals(0, graph.getEdges().size());

    // init the graph
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hb);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
    
    assertEquals(9, graph.getVertices().size());
    assertEquals(edgesToAdd.size(), graph.getEdges().size());

    assertTrue(graph.removeVertex(i));
    try {
      graph.getVertex("i");
      fail("Expected exception");
    }catch(Exception success) {}
    assertEquals(8,graph.getVertices().size());
    assertEquals(edgesToAdd.size()-4, graph.getEdges().size());
    assertEquals(2, graph.getEdgesOf(d).size());
    assertEquals(0, graph.getEdgesOf(f).size());
    assertEquals(2, graph.getEdgesOf(h).size());
    assertEquals(1, graph.getEdgesOf(e).size());
    
    try {
      graph.removeVertex(i);
      fail("Excepted exception");
    }catch(Exception success) {  }
    
    assertTrue(graph.removeVertex(a));
    assertEquals(7,graph.getVertices().size());
    assertEquals(3, graph.getEdges().size());
    assertEquals(0, graph.getEdgesOf(e).size());
    
    graph.removeVertices(graph.getVertices());
    
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
  }

  public void testRemoveVertices() {
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hb);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
    
    assertEquals(9, graph.getVertices().size());
    assertEquals(11, graph.getEdges().size());
    
    graph.removeVertices(Collections.singleton(i));
    
    assertEquals(8, graph.getVertices().size());
    assertEquals(7, graph.getEdges().size());
    
    graph.removeVertices(graph.getVertices());
    
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
  }
  
  public void testGetSelfEdges() {
    logger.info("testGetSelfEdges()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
    
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hb);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
    
    Collection aSelfEdges = graph.getSelfEdgesOf(a);
    assertTrue(aSelfEdges.contains(aa));
    assertEquals(2,aSelfEdges.size());
  }
  public void testGetSourceEdges() {
    logger.info("testGetSourceEdges()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
    
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hb);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
    
    Collection aSourceEdges = graph.getSourceEdgesOf(a);
    assertEquals(1,aSourceEdges.size());
    assertTrue(aSourceEdges.contains(ah));
    
    Collection hSourceEdges = graph.getSourceEdgesOf(h);
    assertEquals(2,hSourceEdges.size());
    assertTrue(hSourceEdges.contains(hb));
    assertTrue(hSourceEdges.contains(hi));
  }
  
  public void testGetDestinationEdges() {
    logger.info("testGetDestinationEdges()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
    
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hb);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
    
    Collection aDestination = graph.getDestinationEdgesOf(a);
    assertEquals(1,aDestination.size());
    assertTrue(aDestination.contains(ea));
    
    Collection iDestination = graph.getDestinationEdgesOf(i);
    assertEquals(3,iDestination.size());
    assertTrue(iDestination.contains(di));
    assertTrue(iDestination.contains(hi));
    assertTrue(iDestination.contains(fi));
    
  }
  public void testGetDegreeOf() {
    logger.info("testGetEdges()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
    
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hb);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
    
    assertEquals(4,graph.getDegreeOf(a));
    assertEquals(1,graph.getDegreeOf(b));
    assertEquals(3,graph.getDegreeOf(d));
    assertEquals(1,graph.getDegreeOf(f));
    assertEquals(4,graph.getDegreeOf(i));
  }

  // accessor methods	
  public void testGetEdges() {
    logger.info("testGetEdges()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
    
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hb);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
    
    assertTrue(graph.getEdges().containsAll(edgesToAdd));
  }

  public void testGetVertex() {
    logger.info("testGetVertex()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
    
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hb);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
    
    Vertex gotI = graph.getVertex(i);    
    assertEquals(i,gotI);
    assertSame(i,gotI);
    
    try {
      Vertex v = graph.getVertex("DOESN'T EXIST");
      fail("expected exception "+v);
    }catch(Exception success) {}

  }

  public void testGetVertices() {
    logger.info("testGetVertices()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
    
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hb);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
    
    Collection v = graph.getVertices();;
    assertTrue(v.contains(a));
    assertTrue(v.contains(b));
    assertTrue(v.contains(c));
    assertTrue(v.contains(d));
    assertTrue(v.contains(e));
    assertTrue(v.contains(f));
    assertTrue(v.contains(g));
    assertTrue(v.contains(h));
    assertTrue(v.contains(i));
    
    assertEquals(9,graph.getVertices().size());

  }

  public void testGetNeighborsOf() {
    logger.info("testGetNeighborsOf()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
    
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hb);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
    
    Collection aNeighbors = graph.getNeighborsOf(a);
    if(logger.isDebugEnabled()) {
      logger.debug("aNeighbors: "+aNeighbors);
    }
    assertEquals(4,aNeighbors.size());
    assertTrue(aNeighbors.contains(a)); // there are two a's
    assertTrue(aNeighbors.contains(h));
    assertTrue(aNeighbors.contains(e));
    
    Collection iNeighbors = graph.getNeighborsOf(i);
    assertEquals(4,iNeighbors.size());
    assertTrue(iNeighbors.contains(d));
    assertTrue(iNeighbors.contains(f));
    assertTrue(iNeighbors.contains(h));
    assertTrue(iNeighbors.contains(e));
    
    graph.removeEdge(di);
    iNeighbors = graph.getNeighborsOf(i);
    assertEquals(3,iNeighbors.size());
    assertFalse(iNeighbors.contains(d)); // we removed this one
    assertTrue(iNeighbors.contains(f));
    assertTrue(iNeighbors.contains(h));
    assertTrue(iNeighbors.contains(e));
    
    // ensure i is updated after the removeEdge
    assertEquals(3,iNeighbors.size());
  }

  public void testGetEdgesOf() {
    logger.info("testGetEdgesOf()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
    
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hb);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
    
    Collection edgesOfA = graph.getEdgesOf(a);
    assertEquals(4,edgesOfA.size());
    assertTrue(edgesOfA.contains(aa));
    assertTrue(edgesOfA.contains(aa2)); // same as aa though
    assertTrue(edgesOfA.contains(ah));
    assertTrue(edgesOfA.contains(ea));
    
    graph.removeEdge(aa);
    
    edgesOfA = graph.getEdgesOf(a);
    assertEquals(3,edgesOfA.size());    
    assertTrue(edgesOfA.contains(aa2)); // same as aa though
    assertTrue(edgesOfA.contains(ah));
    assertTrue(edgesOfA.contains(ea));
    
    graph.removeEdge(ea);
    
    edgesOfA = graph.getEdgesOf(a);
    assertEquals(2,edgesOfA.size());    
    assertTrue(edgesOfA.contains(aa2)); // same as aa though
    assertTrue(edgesOfA.contains(ah));
    
    graph.removeEdges(graph.getEdgesOf(a));
    assertTrue(graph.getEdgesOf(a).isEmpty());
    
    // ensure other vertices get vertices are alright
    assertEquals(1,graph.getEdgesOf(c).size());
    assertEquals(3,graph.getEdgesOf(d).size());
    assertEquals(1,graph.getEdgesOf(e).size());
    assertEquals(1,graph.getEdgesOf(f).size());
    assertEquals(1,graph.getEdgesOf(g).size());
    assertEquals(2,graph.getEdgesOf(h).size());
    assertEquals(4,graph.getEdgesOf(i).size());

  }

  public void testIsVertexPresent() {
    logger.info("testIsVertexPresent()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
    
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
    
    assertTrue(graph.isVertexPresent(a));
    assertFalse(graph.isVertexPresent(b));
    assertTrue(graph.isVertexPresent(c));
    assertTrue(graph.isVertexPresent(d));
    
    assertTrue(graph.removeVertex(a));
    assertFalse(graph.isVertexPresent(a));
    assertTrue(graph.isVertexPresent(c));
    assertTrue(graph.isVertexPresent(d));

  }

  public void testIsEdgePresent() {
    logger.info("testIsEdgePresent()");
    // check the initial state
    assertTrue(graph.getEdges().isEmpty());
    assertTrue(graph.getVertices().isEmpty());
    
    Collection edgesToAdd = new LinkedList();
    edgesToAdd.add(aa);
    edgesToAdd.add(aa2);
    edgesToAdd.add(ah);
    edgesToAdd.add(cd);
    edgesToAdd.add(dg);
    edgesToAdd.add(di);
    edgesToAdd.add(ea);
    edgesToAdd.add(fi);
    edgesToAdd.add(hb);
    edgesToAdd.add(hi);
    edgesToAdd.add(ie);
    graph.addEdges(edgesToAdd);
    
    assertTrue(graph.isEdgePresent(aa));
    assertFalse(graph.isEdgePresent(be));
    assertTrue(graph.isEdgePresent(ah));
    assertTrue(graph.isEdgePresent(cd));
    assertTrue(graph.isEdgePresent(ea));
    assertTrue(graph.isEdgePresent(fi));
    assertTrue(graph.isEdgePresent(ie));
    
    assertTrue(graph.removeEdge(aa));
    assertEquals(3,graph.getEdgesOf(a).size());
    assertTrue(graph.isEdgePresent(aa2));
    
    
  }

}