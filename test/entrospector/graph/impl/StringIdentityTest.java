/*
 * 
 */
package entrospector.graph.impl;

import org.apache.log4j.Logger;

import entrospector.graph.interfaces.Identity;
import junit.framework.TestCase;

/**
 * @author rwinch
 */
public class StringIdentityTest extends TestCase {
  private static final Logger logger = Logger.getLogger(StringIdentityTest.class);
  
  private Identity a,a2, b, m, r, abc, acc, _p, a_p, _e;
  
  public void setUp() {
    a = new StringIdentity("a");
    a2 = new StringIdentity("a");
    b = new StringIdentity("b");
    m = new StringIdentity("m");
    r = new StringIdentity("r");
    abc = new StringIdentity("abc");
    acc = new StringIdentity("acc");
    _p = new StringIdentity(".");
    a_p = new StringIdentity("a.");
    _e = new StringIdentity("");
  }
  
  public void tearDown() {
    a = a2 = b = m = r = abc = acc = _p = a_p = _e = null;
  }

  public boolean isAGreaterThanB(Comparable a, Comparable b) {    
    int result = a.compareTo(b);
    if(logger.isDebugEnabled()) {
	    logger.debug("isGreater("+a+","+b+")");
	    logger.debug("result="+result);
    }
    return result > 0;
  }
  public boolean isBLessThanA(Comparable a, Comparable b) {
    return b.compareTo(a) < 0;
  }  
  public void testEqual() {
    assertEquals(a,"a");
    assertEquals(a,a2);
  }
  public void testGreater() {
    assertTrue(isAGreaterThanB(b,a));
    assertTrue(isAGreaterThanB(r,a));
    assertTrue(isAGreaterThanB(m,b));
    assertTrue(isAGreaterThanB(abc,a));
    assertTrue(isAGreaterThanB(abc,"aab"));
  }
  public void testLess() {
    assertTrue(isBLessThanA(b,a));
    assertTrue(isBLessThanA(r,a));
    assertTrue(isBLessThanA(m,b));
    assertTrue(isBLessThanA(abc,a));
    assertTrue(isBLessThanA("def",abc));
  }
  
  public void testNext() {
    assertEquals(new StringIdentity("abd"),abc.next());
    assertEquals(new StringIdentity("c"),b.next());
    assertEquals(new StringIdentity("0"),_e.next());
    assertEquals(new StringIdentity("00"),_p.next());
    assertEquals(new StringIdentity("b0"),a_p.next());
  }
}
