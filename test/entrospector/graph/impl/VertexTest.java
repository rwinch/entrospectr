/*
 * Intended to test anything about vertex that is independant of graph
 */
package entrospector.graph.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.graph.impl.VertexImpl;
import entrospector.graph.interfaces.Vertex;
import junit.framework.TestCase;

/**
 * @author rwinch
 */
public class VertexTest extends TestCase {
  private static Logger logger = Logger.getLogger(VertexTest.class);
  
  private Vertex a,a2,b,_1;
  
  public void setUp() {
    a = VertexImpl.create("a");
    a2 = VertexImpl.create("a");
    b = VertexImpl.create("b");
    _1 = VertexImpl.create("1");
  }
  public void tearDown() {
    a = b = null;    
  }
  
  public void testEquals() {    
    logger.info("VertexTest.testCompare()");
    assertEquals(a,a);
    assertEquals(a,a2);
    assertEquals(a,"a");
    assertEquals(_1,VertexImpl.create("1"));
  }
  public void testCompareTo() {
    assertTrue(a.compareTo(b) < 0);
    assertTrue(b.compareTo(a) > 0);
  }
  
  public void testClone() {
    Vertex cloneA = (Vertex)a.clone();
    assertEquals(a,cloneA);
  }
  
  public void testMap() {
    Map vertices = new HashMap();
    Object value = new Double(1);
    Vertex __1 = VertexImpl.create("1");
    vertices.put(_1,value);
    assertEquals(value,vertices.get(_1));
    assertEquals(value,vertices.get(__1));
  }
}
