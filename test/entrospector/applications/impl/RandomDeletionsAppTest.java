/*
 * 
 */
package entrospector.applications.impl;


import org.apache.log4j.Logger;

import entrospector.Utilities;
import entrospector.algs.impl.*;

/**
 * A class used to test the RadomDeletionsApp / RandomDeletionsAlg
 * @author rwinch
 */
public class RandomDeletionsAppTest extends AbstractDeletionsTest {
  private Logger logger = Logger.getLogger(RandomDeletionsAppTest.class);
  
  public void testRandDeletions() throws Exception {
    input(g1,"networkCnt1.txt");        
    verticesToDelete = new Object[] {
       _4,_5,_10
    };

    RandomDeletionsAlg.Result result = 
        (RandomDeletionsAlg.Result) randDelAlg.run(
        Utilities.arrayToMap(
            new Object[] {
               "deletionCount",new Integer(verticesToDelete.length),
               "graph",g1 }
        )
    );
    
    g2 = result.getGraph();
    
    StatsAlg.Result statsAlg = StatsAlg.go(
  	    Utilities.arrayToMap(
  	        new Object[] {
            "origionalGraph", g1,
            "newGraph", g2 }
  	    )
    );
    
    if(logger.isDebugEnabled()){
      logger.debug("Filtered: "+statsAlg.getHFlatResult().getFilteredGraphs());
      logger.debug("Retained: "+statsAlg.getHFlatResult().getRetainedGraphs());
    }
    
    // check the number of verticies
    assertEquals(10,g2.getVertices().size());
    // check the number of networks
    assertEquals(6,statsAlg.getNetworkCnt());
    // check the number of filtered networks (not used in hFlat)
    assertEquals(4,statsAlg.getHFlatResult().getFilteredGraphs().size());
    // check the number of retained networks (used in hFlat)
    assertEquals(2,statsAlg.getHFlatResult().getRetainedGraphs().size());
    // check hFlat
    assertEquals(4.23998453277,statsAlg.getHFlatResult().getHFlat().doubleValue(),PRECISSION);
    // check hStat
    assertEquals(4,statsAlg.getHFlatResult().getHStat().doubleValue(),PRECISSION);
    // check the number of verticies that changed hStat values
    assertEquals(6,statsAlg.getVDeltaHStatCnt());
    
    double nodeCnt = g1.getVertices().size() - verticesToDelete.length;
    double expectedVDeltaHStatPercent = 6/nodeCnt;
    // check the percentage of verticies that changed hStat values
    assertEquals(expectedVDeltaHStatPercent,statsAlg.getVDeltaHStatPercent(),PRECISSION);
  }
  
  
}
