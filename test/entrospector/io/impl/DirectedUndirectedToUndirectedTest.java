/*
 * Created on Feb 26, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package entrospector.io.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import org.apache.log4j.Logger;

import entrospector.AbstractEntrospectorTest;
import entrospector.graph.impl.UndirectedEdgeImpl;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Graph;
import entrospector.io.interfaces.GraphFileReader;

/**
 * @author rwinch
 */
public class DirectedUndirectedToUndirectedTest extends AbstractEntrospectorTest {
  private static final Logger logger = Logger.getLogger(DirectedUndirectedToUndirectedTest.class);
  
  private Graph graph;
  private GraphFileReader gfr;
  private DirectedUndirectedToUndirected.Result algResult;
  
  public void setUp() {    
    graph = new UndirectedGraphImpl();    
  }
  
  public void tearDown() {
    graph = null;
  }
  
  public void readGraph(String file) throws IOException, MalformatedException {
    gfr = new DirectedUndirectedToUndirected(getTestFile(file),graph);
    this.algResult = (DirectedUndirectedToUndirected.Result) gfr.read();
  }
  
  public void testInvalidLine() throws Exception {
    logger.info("testInvalidLine()");
    try {
      readGraph("InvalidLine.txt");
      fail("Error: Expected malformated line");
    }catch (MalformatedException success){
      // success
      if(logger.isDebugEnabled()) {
        logger.debug("Success: ",success);
      }
    }    
  }
  
  public void testUncomplimentedEdge() throws Exception {
    logger.info("testUncomplimentedEdge()");    
    
    try {
      readGraph("UncomplimentedEdge.txt");
      fail("Error: Expected uncomplimented edges");
    }catch (RuntimeException success){
      // success
      if(logger.isDebugEnabled()) {
        logger.debug("Success: ",success);
      }
    }
  }
  
  public void testBasicNumberGraph() throws Exception {
    logger.info("testBasicNumberGraph()");
    readGraph("BasicNumberGraph.txt");
    performBasicNumberChecks();
  }
  
  public void performBasicNumberChecks() {
    Collection edges = graph.getEdges();
    Collection expectedEdges = new LinkedList();
    expectedEdges.add(UndirectedEdgeImpl.create("1","1"));
    expectedEdges.add(UndirectedEdgeImpl.create("1","2"));
    expectedEdges.add(UndirectedEdgeImpl.create("1","3"));
    expectedEdges.add(UndirectedEdgeImpl.create("1","4"));
    expectedEdges.add(UndirectedEdgeImpl.create("1","5"));
    expectedEdges.add(UndirectedEdgeImpl.create("2","1"));
    expectedEdges.add(UndirectedEdgeImpl.create("3","1"));
    expectedEdges.add(UndirectedEdgeImpl.create("4","1"));
    expectedEdges.add(UndirectedEdgeImpl.create("5","1"));
    assertTrue(edges.containsAll(expectedEdges));
    assertEquals(5,algResult.getSelfEdges().size());
  }
  
  public void testBlankLines() throws Exception {
    logger.info("testBlankLines()");
    readGraph("BlankLines.txt");
    performBasicNumberChecks();
  }
  
  public void testComments() throws Exception {
    logger.info("testComments()");
    readGraph("Comments.txt");
    performBasicNumberChecks();
  }
  
  public void testBasicStringGraph() throws Exception {
    logger.info("testBasicStringGraph()");
    readGraph("BasicStringGraph.txt");
    Collection edges = graph.getEdges();
    Collection expectedEdges = new LinkedList();
    expectedEdges.add(UndirectedEdgeImpl.create("a","a"));
    expectedEdges.add(UndirectedEdgeImpl.create("a","b"));
    expectedEdges.add(UndirectedEdgeImpl.create("a","c"));
    expectedEdges.add(UndirectedEdgeImpl.create("a","d"));
    expectedEdges.add(UndirectedEdgeImpl.create("a","e"));
    expectedEdges.add(UndirectedEdgeImpl.create("b","a"));
    expectedEdges.add(UndirectedEdgeImpl.create("c","a"));
    expectedEdges.add(UndirectedEdgeImpl.create("d","a"));
    expectedEdges.add(UndirectedEdgeImpl.create("e","a"));
    assertTrue(edges.containsAll(expectedEdges));
    assertEquals(5,algResult.getSelfEdges().size());    
  }
  public void testBasicMixedVertexGraph() throws Exception {
    logger.info("testBasicMixedVertexGraph()");    
    readGraph("BasicMixedVertexGraph.txt");
    assertTrue(graph.isEdgePresent(UndirectedEdgeImpl.create("2","2")));  
  }
  public void testBasicMixedEdgeGraph() throws Exception {
    logger.info("testBasicMixedEdgeGraph()");    
    readGraph("BasicMixedEdgeGraph.txt");    
    assertTrue(graph.isEdgePresent(UndirectedEdgeImpl.create("b","2")));
  }

}
