/*
 * 
 */
package entrospector.applications.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.ArgParser;
import entrospector.Utilities;
import entrospector.algs.impl.DegenerationAlg;
import entrospector.algs.impl.DuplicationAlg;
import entrospector.algs.impl.StatsAlg;
import entrospector.applications.interfaces.Application;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Graph;
import entrospector.io.impl.DirectedUndirectedToUndirected;
import entrospector.io.impl.FileWriterImpl;
import entrospector.io.interfaces.FileWriter;
import entrospector.io.interfaces.GraphFileReader;

/**
 * @author rwinch
 */
public class DuplicationDegenerationApp extends AbstractApplication {
  private static Logger logger = Logger.getLogger(DuplicationDegenerationApp.class);

  private String inputFile, outputPath;

  // number of iterations
  private int iterations;

  // the percent of verticies to duplicate
  private double startDuplicationPercent,endDuplicationPercent;
  
  private double duplicationPercentStep;
  
  // the percent of edges to be redundant
  private double redundancyPercent;

  /* this will be our graph */
  private Graph graph;
  
  boolean isNewDuplication;

  private int strength = 3;

  // graph reader
  private GraphFileReader gfr;

  private FileWriter writer;

  public void init(ArgParser argParser) {
    iterations = argParser.getInt("iterations");
    startDuplicationPercent = argParser.getDouble("start duplication percent");
    endDuplicationPercent = argParser.getDouble("end duplication percent");
    duplicationPercentStep = argParser.getDouble("duplication percent step");
    redundancyPercent = argParser.getDouble("redundancy percent");
    outputPath = argParser.getString("outputPath");
    inputFile = argParser.getString("inputFile");
    isNewDuplication = argParser.isFlagSet("isNewDuplication");
    
    if(logger.isDebugEnabled()) {
      logger.debug("isNewDuplication="+isNewDuplication);
      logger.debug("isSet="+argParser.isFlagSet("isNewDuplication"));
    }

    // init the 
    graph = new UndirectedGraphImpl();
    gfr = new DirectedUndirectedToUndirected(inputFile, graph);
  }

  public Object run() throws Exception {
    logger.info("reading graph");
    // read in the graph
    gfr.read();
    for(double duplicationPercent=startDuplicationPercent;
    		duplicationPercent<=endDuplicationPercent;
    		duplicationPercent+=duplicationPercentStep) {
	    int duplicationCount = (int) (duplicationPercent*gfr.getGraph().getVertices().size());
	    if(logger.isDebugEnabled()) {
	      logger.debug("duplicationCount="+duplicationCount);
	    }
	    Map origionalHStat = StatsAlg.getOrigionalHStat(graph);
	    writer = new FileWriterImpl(this.outputPath+
);
	    writer.write(StatsAlg.Result.getHeader());
	    // duplication
	    DuplicationAlg.Result duplicationResult = null;
	    
	    for(int iteration=1;iteration<=iterations;iteration++) {
	      if(logger.isDebugEnabled()) {
	        logger.debug("duplicationCount="+duplicationCount+"; iteration="+iteration);
	      }
	      Graph g = new UndirectedGraphImpl();
	      g.addEdges(graph.getEdges());
	      	      
	      // should we get a new duplication?
	      if(duplicationResult == null || isNewDuplication) {	        
	        duplicationResult = performDuplication(g,duplicationCount);
	      }
	      
	      // degeneration
	      DegenerationAlg.Result degenResult = peformDegeneration(duplicationResult,redundancyPercent);	      	      
	      
	      //  TODO: add who is clone of who and who is cloned from who
	      //  we don't care about this right now though
	      StatsAlg.Result statsAlg = StatsAlg.go(
	          Utilities.arrayToMap(
	              new Object[] {
	                  "origionalHStat",origionalHStat,
	                  "origionalGraph",graph,
	                  "newGraph",degenResult.getGraph()}));
	      
	      writer.write(statsAlg);
	      writer.flush();
	    }
	    writer.close();
  }
    return null;
  }
  public static DuplicationAlg.Result performDuplication(Graph g, int duplicationCount) {
    logger.info("peformDuplication");
    
    Map args = new HashMap();
    args.put("graph",g);
    args.put("duplicationCount",new Integer(duplicationCount));
    
    DuplicationAlg.Result duplication = DuplicationAlg.go(args);    
    return duplication;
  }
  public static DegenerationAlg.Result peformDegeneration(DuplicationAlg.Result duplication,double redundancyPercent) {
    logger.info("peformDegeneration");
    
    int redundancyCnt = (int)(duplication.getOrigionalToDuplicated().size()*redundancyPercent);
    logger.debug("redundancyCnt="+redundancyCnt);
    Map args = new HashMap();
    args.put("graph",duplication.getGraph());
    args.put("orgionalToDuplicated",duplication.getOrigionalToDuplicated());
    args.put("redundancyCnt",new Integer(redundancyCnt));
    DegenerationAlg.Result result = DegenerationAlg.go(args);
    return result;
  }

  public ArgParser createArgParser() {
    ArgParser argParser = new ArgParser();
    argParser.addArgument("iterations", "the number of iterations");
    argParser.addArgument("start duplication percent", "the starting perecent of vertices to duplicate");
    argParser.addArgument("end duplication percent", "the ending perecent of vertices to duplicate (inclusive)");    
    argParser.addArgument("duplication percent step", "the perecent to increment the duplication percent of vertices to duplicate");
    argParser.addArgument("redundancy percent", "the percentage of redundant edges to add");
    argParser.addArgument("inputFile", "the file to input");
    argParser.addArgument("outputPath", "the path to output files to");
    argParser.addFlag("isNewDuplication","if true creates a new duplicated network each time");

    return argParser;
  }
  
  public static void main(String[] args) {
    if(logger.isDebugEnabled()) {
      logger.debug("args="+args.length);
    }
    
    Application app = new DuplicationDegenerationApp();
    ArgParser argParser = app.createArgParser();
    
    try {
      argParser.parse(args);
      app.init(argParser);
      app.run();
    }catch (Exception exception) {
      exception.printStackTrace();
      System.err.println(argParser.getHelp());
    }
    
    System.out.println("----------"+Utilities.EOL+"Program Terminated");        
  }

}