/*
 * 
 */
package entrospector.applications.impl;

import entrospector.applications.interfaces.Application;

/**
 * <p>
 * This class is intended to easily allow changes to where output is redirected
 * to. This will be nice in the event that someone (i.e. Phil) writes a GUI to
 * be used for the application
 * </p>
 * 
 * @author rwinch
 */
public abstract class AbstractApplication implements Application {
  public void print(Object o) {
    System.out.print(o);
  }

  public void println(Object o) {
    System.out.println(o);
  }
}