/*
 * 
 */
package entrospector.applications.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.Iterator;

import org.apache.log4j.Logger;

import entrospector.ArgParser;
import entrospector.EntrospectorProperties;
import entrospector.applications.interfaces.Application;

/**
 * @author rwinch
 */
public class CmdLineAppRunner {
  private static Logger logger = Logger.getLogger(CmdLineAppRunner.class);
  private EntrospectorProperties properties;
  private boolean isAlive = true;
  private BufferedReader stdin;
  
  private Class[] programClasses;
  private String[] programDescriptions;

  public CmdLineAppRunner() throws Exception {
    this(EntrospectorProperties.getDefault().getProperty("entrospector.CmdLineAppRunner.properties"));
  }
  
  public CmdLineAppRunner(String fileName) throws Exception {
    properties = new EntrospectorProperties(fileName);
    if(logger.isDebugEnabled()) {
      logger.debug("propFileName = "+fileName);    
    }
    stdin =  new BufferedReader(new InputStreamReader(System.in));
    loadPrograms();
  }
  
  protected void loadPrograms() throws Exception {
    programClasses = new Class[properties.size()];
    programDescriptions = new String[properties.size()];
    
    int index = 0;
    Iterator iProgramClassNames = properties.keySet().iterator();    
    while(iProgramClassNames.hasNext()) {
      String className = (String)iProgramClassNames.next();
      programClasses[index] = Class.forName(className);
      programDescriptions[index] = properties.getProperty(className);
      
      index++;
    }
  }
    
  public void run() throws Exception {
    while(isAlive) {
	  int command = 0;
	  try {
	    command = nextChoice();
	  }catch(Exception exception) {
	    logger.error("Couldn't get command: ",exception);
	  }      
      if(-1 == command) {
        isAlive = false;
      }else {
        try {
          runProgram(command);
        }catch(Exception e) {
          logger.error("Error occured while attempting to run program",e);
        }
      }
    }
  }
  
  public int nextChoice() throws IOException {
    printMenu();
    System.out.flush();
    String choice = stdin.readLine();
    return Integer.parseInt(choice);
  }
  
  public void printMenu() {
    for(int index=0;index<programDescriptions.length;index++) {
      System.out.println((index+1)+")\t"+programDescriptions[index]);
    }   
    
    System.out.print("Enter Your Choice: ");    
    System.out.flush();
  }
  
  public void runProgram(int index) throws Exception {
    index -= 1;
    Class program = programClasses[index];
    Application app = (Application) program.newInstance();
        
    ArgParser argParser = app.createArgParser();    
    argParser.promptForValues();
    
    app.init(argParser);
    
    app.run();   
  }
  
  public static void main(String[] args) throws Exception {
    
    CmdLineAppRunner program;
    if(args == null || args.length < 1) {
      logger.info("using default");
      program = new CmdLineAppRunner();
    }else {
      if(logger.isDebugEnabled()) {
        logger.debug("using "+args[0]);
      }
      program = new CmdLineAppRunner(args[0]);
    }
    
    program.run();
    
  }
}
