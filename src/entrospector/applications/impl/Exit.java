/*
 * 
 */
package entrospector.applications.impl;

import entrospector.ArgParser;
import entrospector.Utilities;

/**
 * Calls system.exit(0); for exiting application runner
 * 
 * @author rwinch
 */
public class Exit extends AbstractApplication {
  
  /* (non-Javadoc)
   * @see entrospector.applications.interfaces.Application#run()
   */
  public Object run() throws Exception {
    System.out.println("------"+Utilities.EOL+"Program Terminated");
    System.exit(0);
    return null;
  }
  public void init(ArgParser argParser) {
  }
  
  public ArgParser createArgParser() {
    ArgParser argParser = new ArgParser();
    return argParser;
  }

}
