/*
 * 
 */
package entrospector.applications.impl;

import org.apache.log4j.Logger;

import entrospector.ArgParser;
import entrospector.Utilities;
import entrospector.algs.impl.HFlatAlg;
import entrospector.applications.interfaces.Application;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Graph;
import entrospector.io.impl.DirectedUndirectedToUndirected;
import entrospector.io.interfaces.GraphFileReader;

/**
 * @author rwinch
 */
public class NetworkCountApp extends AbstractApplication implements Application {
  private static final Logger logger = Logger.getLogger(NetworkCountApp.class);
  
  private GraphFileReader gfr;
  private Graph graph = new UndirectedGraphImpl();
  
  /* (non-Javadoc)
   * @see entrospector.applications.interfaces.Application#init(entrospector.ArgParser)
   */
  public void init(ArgParser argParser) {
    String fileName = argParser.getString("inputFile");
    gfr = new DirectedUndirectedToUndirected(fileName,graph);
  }

  /* (non-Javadoc)
   * @see entrospector.applications.interfaces.Application#createArgParser()
   */
  public ArgParser createArgParser() {
    ArgParser result = new ArgParser();
    result.addArgument("inputFile","the graph to input");
    return result;
  }

  /* (non-Javadoc)
   * @see entrospector.applications.interfaces.Application#run()
   */
  public Object run() throws Exception {
    logger.info("reading graph...");
    gfr.read();
    
    HFlatAlg.Result result = HFlatAlg.go(Utilities.arrayToMap(
        new Object[] {"graph",graph}));
    
    System.out.println("Filtered.size="+result.getFilteredGraphs().size());
    System.out.println("Retained.size="+result.getRetainedGraphs().size());
    System.out.println("HFlat="+result.getHFlat());
    System.out.println("HStat="+result.getHStat());
    System.out.println("Vertices: "+graph.getVertices().size());
    System.out.println("Edges: "+graph.getEdges().size());
    return null;
  }

}
