/*
 * 
 */
package entrospector.applications.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.ArgParser;
import entrospector.Utilities;
import entrospector.applications.interfaces.Application;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;
import entrospector.io.impl.DirectedUndirectedToUndirected;
import entrospector.io.interfaces.GraphFileReader;

/**
 * @author rwinch
 */
public class VertexConnections extends AbstractApplication implements Application {
  private static final Logger logger = Logger.getLogger(VertexConnections.class);
  private static final Integer ZERO = new Integer(0);
  
  private Map edgeCounts = new HashMap();
  
  private GraphFileReader gfr;
  private Graph graph = new UndirectedGraphImpl();
  
  /* (non-Javadoc)
   * @see entrospector.applications.interfaces.Application#init(entrospector.ArgParser)
   */
  public void init(ArgParser argParser) {
    String fileName = argParser.getString("inputFile");
    gfr = new DirectedUndirectedToUndirected(fileName,graph);
  }

  /* (non-Javadoc)
   * @see entrospector.applications.interfaces.Application#createArgParser()
   */
  public ArgParser createArgParser() {
    ArgParser result = new ArgParser();
    result.addArgument("inputFile","the graph to input");
    return result;
  }

  /* (non-Javadoc)
   * @see entrospector.applications.interfaces.Application#run()
   */
  public Object run() throws Exception {
    logger.info("reading graph...");
    gfr.read();
    
    Iterator iVertices = graph.getVertices().iterator();
    while(iVertices.hasNext()) {
      Vertex vertex = (Vertex)iVertices.next();
      Integer degree = new Integer(graph.getDegreeOf(vertex));
      inc(degree);
    }

    List degrees = new LinkedList(edgeCounts.keySet());
    Collections.sort(degrees);
    Iterator iDegrees = degrees.iterator();
    int totalVertices = 0;
    double bitTotal = 0;
    while(iDegrees.hasNext()) {
      Integer degree = (Integer)iDegrees.next();
      Integer count = (Integer) edgeCounts.get(degree);
      double bit = Utilities.logOfBase(degree.intValue(),2);
      double bitSubTotal = bit*count.intValue();
      bitTotal += bitSubTotal;
      println("Count of Vertices with degree "+degree+" = "+count+" bit="+bit+" total bits="+bitSubTotal);
      totalVertices += count.intValue();
    }
    println("Total Vertices = "+totalVertices);
    println("Total Bits = "+bitTotal);
    return null;
  }
  
  public void inc(Integer index) {
    
    Object value = edgeCounts.get(index);
    if(value==null) {
      value = ZERO;
    }
    Integer v = (Integer)value;
    edgeCounts.put(index,new Integer(v.intValue()+1));       
  }

}
