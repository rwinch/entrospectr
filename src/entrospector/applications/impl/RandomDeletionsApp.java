/*
 * 
 */
package entrospector.applications.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.ArgParser;
import entrospector.Utilities;
import entrospector.algs.impl.RandomDeletionsAlg;
import entrospector.algs.impl.RandomizeVerticesAlg;
import entrospector.algs.impl.StatsAlg;
import entrospector.algs.interfaces.Algorithm;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Graph;
import entrospector.io.impl.DirectedUndirectedToUndirected;
import entrospector.io.impl.FileWriterImpl;
import entrospector.io.interfaces.FileWriter;
import entrospector.io.interfaces.GraphFileReader;

/**
 * @author rwinch
 */
public class RandomDeletionsApp extends AbstractApplication {
  
  private static Logger logger = Logger.getLogger(RandomDeletionsApp.class);
    
  private String inputFile, outputPath;
  
  // number of iterations
  private int iterations;

  // the step size for deletions
  private double deletionStep;

  // start percentage of deletions
  private double deletionStart;

  // the end percentage for the number of deletions
  private double deletionEnd;

  /* this will be our graph */
  private Graph graph;
  // mapping of origional H-Stat for each vertex to compare after deletions
  private Map origionalHStat;
  private int strength = 3;
  
  // graph reader
  private GraphFileReader gfr;
  private FileWriter writer;
  
  private Algorithm randVertices = new RandomizeVerticesAlg();
  
  public void init(ArgParser argParser) {
    iterations = argParser.getInt("iterations");
    deletionStart = argParser.getDouble("start");
    deletionStep = argParser.getDouble("step");
    deletionEnd = argParser.getDouble("stop");
    outputPath = argParser.getString("outputPath");
    inputFile = argParser.getString("inputFile");
    
    // init the graph
    graph = new UndirectedGraphImpl();    
    gfr = new DirectedUndirectedToUndirected(inputFile,graph);   
    origionalHStat = new HashMap();
  }
  
  public Object run() throws Exception {
    logger.info("reading graph");
    // read in the graph
    gfr.read();
        
    int vCount = graph.getVertices().size();
    if(logger.isDebugEnabled()) {
      logger.debug("vertex count="+vCount);
    }
           
    // initalize our origional
    origionalHStat = StatsAlg.getOrigionalHStat(graph);
   
    // perform between deletionStart and deletionStop deletions and 
    // incrementing deletion percent by deletionStep iteration types
    logger.info("start deletions...");
    for(double delPercent=deletionStart;delPercent<=deletionEnd;delPercent+=deletionStep) {
      int delCount = (int)(delPercent*vCount);
      if(logger.isDebugEnabled()) {
        logger.debug("starting "+(delPercent*100)+"% = "+delCount+".");
      }
      if(vCount >= delCount) {
        String outputFileName = this.outputPath + "RandomDeletions_" + delPercent + "_" + iterations+".dat";
        writer = new FileWriterImpl(outputFileName);
        
        // write out a header
        writer.write(StatsAlg.Result.getHeader());
      
        for(int i=1;i<=iterations;i++) {
          if(logger.isDebugEnabled()) {
            logger.debug("iteration: "+i+" of "+iterations);
          }
          // randomly delete and get result
          RandomDeletionsAlg.Result delResult = RandomDeletionsAlg.go(
              Utilities.arrayToMap(
                  new Object[] {"deletionCount",new Integer(delCount),"graph",graph}));
          
          StatsAlg.Result statsAlg = StatsAlg.go(
              Utilities.arrayToMap(
                  new Object[] {"origionalHStat",origionalHStat,
                      "origionalGraph",graph,"newGraph",delResult.getGraph()}));
          
          writer.write(statsAlg);
          writer.flush();
          System.gc();
        }
        writer.close();
      }else {
        logger.error("Can't delete "+delCount+" vertices with only "+vCount+" vertices present...skipping");
      }
    }    
    return null;
  }

  public ArgParser createArgParser() {

    ArgParser argParser = new ArgParser();
    argParser.addArgument("iterations","the number of iterations");
    argParser.addArgument("start","the amount deletion percentage starts at");
    argParser.addArgument("step","the amount to increase deletion percentage");
    argParser.addArgument("stop","the amount deletion percentage stops at");
    argParser.addArgument("inputFile","the file to input");
    argParser.addArgument("outputPath","the path to output files to");
    
    return argParser;
  }
  
}
