/*
 *
 */
package entrospector.applications.interfaces;

import entrospector.ArgParser;

/**
 * @author rwinch
 */
public interface Application {
  /**
   * Initialize the algorithm
   * @param argParser
   */
  void init(ArgParser argParser);
  /**
   * Create an argParser for this algorithm (how we get the parameters)
   * @return
   */
  ArgParser createArgParser();
  /**
   * Run the algorithm
   * @return
   * @throws Exception
   */
  Object run() throws Exception;
  
  void println(Object o);
  void print(Object o);
  
}
