/*
 * This class will read an undirected graph represented as a directed graph into an undirected graph
 */
package entrospector.io.impl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import entrospector.Utilities;
import entrospector.graph.impl.IDFactory;
import entrospector.graph.impl.UndirectedEdgeImpl;
import entrospector.graph.impl.VertexImpl;
import entrospector.graph.interfaces.Edge;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;
import entrospector.io.interfaces.GraphFileReader;

/**
 * @author rwinch
 */
public class DirectedUndirectedToUndirected extends AbstractGraphFileReader implements GraphFileReader {
  private static Logger logger = Logger.getLogger(DirectedUndirectedToUndirected.class);
  
  public class Result {
    /**
     * Edges that have their compliment has been found
     */
    private List complimentedEdges = new LinkedList();
    
    /**
     * Collection of the selfEdges
     */
    private List selfEdges = new LinkedList();
    
    /** 
     * Edges that have been encountered once (ie looking for their compliments still)
     * This does not include selfEdges 
     */
    private Set encounteredEdges = new TreeSet();

    public Result(List complimentedEdges, List selfEdges, Set encounteredEdges) {
      setComplimentedEdges(complimentedEdges);      
    }
    
    public void setComplimentedEdges(List c) {
      this.complimentedEdges = c;
    }
    public void setSelfEdges(List c) {
      this.selfEdges = c;
      
    }
    public void setEncounteredEdges(Set s) {
      this.encounteredEdges = s;
    }
    
    public List getComplimentedEdges() {
      return complimentedEdges;
    }
    public List getSelfEdges() {
      return selfEdges;
    }
    public Set getEncounteredEdges() {
      return encounteredEdges;
    }
  }
  
  public DirectedUndirectedToUndirected(String fileName, Graph graph) {
    super(fileName,graph);
  }
 
  /*  (non-Javadoc)
   * @see entrospector.io.interfaces.GraphFileReader#read()
   */
  public Object read() throws IOException, MalformatedException {
    // init our result
    Result result = new Result(new LinkedList(),new LinkedList(),new TreeSet());
    
    // open an input stream
    FileInputStream stream = new FileInputStream(fileName);
    BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

    // forever (or till we call break)
    while (true) {
      // read the line
      String line = reader.readLine();

      // if the line is null terminate
      if (line == null)
        break;

      // if the line does not start with # and is not empty
      if (!line.startsWith("#") && !"".equals(line)) {
        // attempt to split the line based upon a space
        String l[] = line.split(" ");
        try {
          // nodeA = token1 and nodeB = token2
          String nodeA = l[0];
          String nodeB = l[1];

          // create two new vertices
          Vertex v1 = new VertexImpl(IDFactory.create(nodeA));
          Vertex v2 = new VertexImpl(IDFactory.create(nodeB));

          Edge edge = new UndirectedEdgeImpl(v1,v2);
          if(logger.isDebugEnabled()) {          
            logger.debug("checking "+edge+"...");
          }
          
          // is a self edge
          if(v1.equals(v2)) {
            if(logger.isDebugEnabled()) {
              logger.debug("added self edge: "+edge);
            }
            result.getSelfEdges().add(edge);
          }else {
            // see if it has been encountered
            if(result.getEncounteredEdges().contains(edge)) {
              if(logger.isDebugEnabled()) {
                logger.debug("now is complimented: "+edge);
              }
              // it has been encountered, so remove from encountered and then put in complimented
              result.getEncounteredEdges().remove(edge); // could put this as guard for if statement to improve slightly
              result.getComplimentedEdges().add(edge);
            }else {
              // it hasn't been encountered
              
              result.getEncounteredEdges().add(edge);
              if(logger.isDebugEnabled()) {
                logger.debug("now is encountered: "+edge);
                logger.debug("encountered edges: "+result.getEncounteredEdges());
              }              
            }
          }
        } catch (ClassCastException e) {
          // oops got some sort of error, probably a malformated line
          throw new MalformatedException("Malformated line: '" + line + "'. Expected '<id-1> <id-2>' where id-1 and id-2 are of the same type", e);
        } catch (Exception e) {
          // oops got some sort of error, probably a malformated line
          throw new MalformatedException("Malformated line: '" + line + "'. Expected '<id-1> <id-2>' where id-1 and id-2 are of the same type", e);
        }
      }
    }
    // close the reader
    reader.close();
    
    // check for uncomplimented edges
    if(!result.getEncounteredEdges().isEmpty()) {
      Graph g = (Graph)graph.clone();
      g.addEdges(result.getComplimentedEdges());
      g.addEdges(result.getSelfEdges());
      
      throw new RuntimeException("Error: There are edges that do not have compliments left. " +
          Utilities.EOL + "This does not appear to be an undirected graph represented as a directed graph. " +
          Utilities.EOL + "Edges: " + g.getEdges() +
          Utilities.EOL + "Uncomplimented " +result.getEncounteredEdges());
    }else {
      // create the graph
      try {
        graph.addEdges(result.getComplimentedEdges());
        graph.addEdges(result.getSelfEdges());
      }catch (ClassCastException exception) {
        throw new MalformatedException("Input file contains incomparable types",exception);
      }
    }
    
    return result;
  }
}
