/*
 *
 */
package entrospector.io.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import entrospector.Utilities;
import entrospector.io.interfaces.FileWriter;

/**
 * @author rwinch
 */
public class FileWriterImpl 
  implements FileWriter { 
  
    private FileOutputStream stream;

    private BufferedWriter writer;

    private String fileName;

    public FileWriterImpl(String fileName) throws IOException {
      this.fileName = fileName;
      
      File file = new File(fileName);
      File dir = new File(file.getParent());
      if(!dir.exists()) {
        dir.mkdirs();
        file = file.getAbsoluteFile();
      }
      if(!file.exists()) {
        file.createNewFile();
      }
      
      stream = new FileOutputStream(fileName);
      writer = new BufferedWriter(new OutputStreamWriter(stream));
    }
   
    public void write(Object obj) throws IOException {
      writer.write(String.valueOf(obj));
    }
    public void writeln(Object obj) throws IOException {
      write(obj);
      write(Utilities.EOL);
    }

    public void flush() throws IOException {
      writer.flush();
    }
    public void close() throws IOException {
      writer.close();
    }
    
    public String getFileName() {
      return this.fileName;
    }

}
