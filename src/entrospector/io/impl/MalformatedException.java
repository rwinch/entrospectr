/*
 * Created on Feb 26, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package entrospector.io.impl;

/**
 * @author rwinch
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MalformatedException extends Exception {

  /**
   * @param string
   * @param e
   */
  public MalformatedException(String string, Exception e) {
    super(string,e);
  }

}
