/*
 * Created on Feb 18, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package entrospector.io.impl;

import java.io.IOException;

import entrospector.graph.interfaces.Graph;
import entrospector.io.interfaces.GraphFileReader;

/**
 * @author rwinch
 */
public abstract class AbstractGraphFileReader implements GraphFileReader {
  protected Graph graph;
  
  protected String fileName;
  
  public AbstractGraphFileReader(String fileName, Graph graph) {
    this.graph = graph;
    this.fileName = fileName;
  }


  /* (non-Javadoc)
   * @see entrospector.io.interfaces.GraphFileReader#getFileName()
   */
  public String getFileName() {
    return fileName;
  }

  /* (non-Javadoc)
   * @see entrospector.io.interfaces.GraphFileReader#getGraph()
   */
  public Graph getGraph() {
    return graph;
  }

  /*  (non-Javadoc)
   * @see entrospector.io.interfaces.GraphFileReader#read(java.lang.String, entrospector.graph.interfaces.Graph)
   */
  public Object read(String fileName, Graph graph) throws IOException, MalformatedException {
    this.fileName = fileName;
    this.graph = graph;
    return read();
  }

}
