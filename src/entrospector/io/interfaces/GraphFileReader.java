/*
 * 
 */
package entrospector.io.interfaces;

import java.io.IOException;

import entrospector.graph.interfaces.Graph;
import entrospector.io.impl.MalformatedException;

/**
 * @author rwinch
 */
public interface GraphFileReader {
  Object read() throws IOException, MalformatedException;
  Object read(String fileName, Graph graph) 
    throws MalformatedException, IOException;
  String getFileName();
  Graph getGraph();
}
