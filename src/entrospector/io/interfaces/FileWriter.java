/*
 * 
 */
package entrospector.io.interfaces;

import java.io.IOException;

/**
 * @author rwinch
 */
public interface FileWriter {
  String getFileName();
  void flush() throws IOException;
  void write(Object obj) throws IOException;
  void writeln(Object obj) throws IOException;
  void close() throws IOException;
}
