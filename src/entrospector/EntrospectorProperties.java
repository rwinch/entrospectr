/*
 * 
 */
package entrospector;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.helpers.Loader;

/**
 * This is the basis for the configuration for the entire application. It allows
 * the various parts to be configured by reading in properties files.
 * 
 * Ensure to include the config files in the class path and that the config files
 * are not in a jar.
 * 
 * @author rwinch
 */
public class EntrospectorProperties extends Properties {
  private static final Logger logger = Logger
      .getLogger(EntrospectorProperties.class);

  private File file;

  // where we will look for the config file
  private static final String CONF_DIR = "conf";

  private static Properties defaultProperties = null;
  
  public EntrospectorProperties(String fileName) throws Exception {
    try {
      init(new File(fileName));
    } catch (Exception e) {
      init(new File(new URI(fileName).getPath()));
    }
  }

  public EntrospectorProperties(File file) throws Exception {
    init(file);
  }
  /**
   * Initialize the file name and read in the properties
   * @param file
   * @throws Exception
   */
  private void init(File file) throws Exception {
    if(logger.isDebugEnabled()) {
      logger.debug("EntrospectorProperties( " + file + " )");
    }
    this.file = file;
    load(new FileInputStream(file));
  }
  /**
   * The singleton pattern. Allows a static reference to this class.
   * @return
   */
  public static Properties getDefault() {
    if (defaultProperties == null) {
      try {
        // try and get the default properties file
        defaultProperties = new EntrospectorProperties(
            getDefaultPropertiesFile("entrospector"));
      } catch (Exception exception) {
        logger
            .fatal(
                "Couldn't load the default gridshell properties file.",
                exception);
        System.exit(1);
      }
    }
    return defaultProperties;
  }

  /**
   * Finds the resource file in the following manner:
   * 
   * http://logging.apache.org/log4j/docs/api/org/apache/log4j/helpers/Loader.html#getResource(java.lang.String)
   * 
   * @return
   */
  public static File getDefaultPropertiesFile(String identifier) {
    String systemPropLocation = System.getProperty(identifier+".configuration");
    String resource = identifier + ".properties";
    
    // try and load from system propery which is specified at commandline
    // -D<identifier>.configuration <value>
    File sysPropFile = null;
    try {
      sysPropFile = new File(new URI(systemPropLocation+"/"+resource));
    }catch(Exception e) {
      logger.debug("not uri sysPropFile",e);
      try {
        sysPropFile = new File(systemPropLocation);
      }catch(Exception e2) {
        logger.debug("not relative uri sysPropFile",e2);
      }
    }        
    if(logger.isDebugEnabled()) {
      logger.debug("systemPropFile="+sysPropFile);
    }    
    if(sysPropFile!=null && sysPropFile.exists()) {
      logger.info("exists");
      // if it exists return it
      return sysPropFile;
    }
    
    // try to load the resource using the log4j's class loader  
    URL url = Loader.getResource(resource);
    try {
      if(logger.isDebugEnabled()) {
        logger.debug("url="+url);
      }
      return new File(new URI(url.toString()));
    } catch (Exception e) {
      logger.warn("Couldn't find resource '" + resource
          + ". Ensure it is on the classpath.", e);
    }
    // we couldn't find it return null
    return null;
  }

  /**
   * Resets the default properties to null,
   * this allows you to modify the properties file and reload it
   */
  public static void clearDefaultProperties() {
    defaultProperties = null;
  }

  /**
   * Returns the property's value
   */
  public String getProperty(String propertyName) {
    if(logger.isDebugEnabled()) {
      logger.debug("getProperty( " + propertyName + " )");
    }
    // get the value from our superclass
    String result = super.getProperty(propertyName);
    
    if(logger.isDebugEnabled()) {
      logger.debug("result before: " + result);
	  logger.debug("replacing with " + file);      
    }
    // replace variables w/ their values
    if (result != null) {
      result = result.replaceFirst("\\$\\{this.path\\}", getFile()
          .getParentFile().toURI().toString());
      result = result.replaceFirst("\\$\\{this.path.string\\}", getFile()
          .getParentFile().toString());
    }
    if(logger.isDebugEnabled()) {
      logger.debug("result after: " + result);
    }
    return result;
  }

  public File getFile() {
    return file;
  }
}