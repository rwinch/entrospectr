/*
 * 
 */
package entrospector;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * <p>
 * This class allows programs to read in values from the command line and to
 * prompt users for values. It is composed of three ideas: Option, Flag, and
 * Value.
 * </p>
 * 
 * <h3>Example class that uses an arg parser</h3>
 * 
 * <pre>
 * 
 *  public void class MyClass {
 *  
 *    public ArgParser createArgParser() {
 *      ArgParser result = new ArgParser();
 *      result.addArgument(&quot;option1&quot;,&quot;enter a value for option1&quot;);
 *      result.addArgument(&quot;option2&quot;,&quot;enter a value for option2&quot;);
 *      result.addFlag(&quot;myFlag&quot;,&quot;enter either 'true' or 'false' for this flag&quot;);
 *      return result;
 *    }
 *  } 
 *  
 * </pre>
 * 
 * <h3>Reading from commandline</h3>
 * 
 * <p>
 * The commandline value below states that "option1" will have the value "option
 * one's value" the flag "myFlag" will be true and the option "option2" will be
 * "value 2".
 * </p>
 * 
 * <pre>
 * 
 *  java MyClass -option1 &quot;option one's value&quot; -myFlag -option2 &quot;value 2&quot;
 *  
 * </pre>
 * 
 * Add code to above class
 * 
 * <pre>
 * public static void main(String[] args) throws Exception {
 *   // create the instance
 *   MyClass myClass = new MyClass();
 *   // create an argParser for this class
 *   ArgParser ap = myClass.createArgParser();
 *   // parse the values
 *   ap.parse(args);
 *   // access a value
 *   ap.getString(&quot;option2&quot;); // would be &quot;value 2&quot; 
 * }
 * </pre>
 * 
 * <h3>Prompting for values</h3>
 * 
 * <p>
 * If a class has an ArgParser associated with it, it may request resources from
 * another program
 * </p>
 * 
 * <pre>
 * public class AnotherClass {
 *   public void initMyClass() {
 *     // we don't know what &quot;MyClass&quot; needs for input, but it does
 *     // create the instance
 *     MyClass myClass = new MyClass();
 *     // create an argParser for this class (it knows what values the class needs)
 *     ArgParser ap = myClass.createArgParser();
 *     // this will allow you to enter the values in the terminal
 *     ap.promptForValues();
 *     // this method should use the argparser values to initialize itself
 *     myClass.init(ap);
 *   }
 * }
 * </pre>
 * 
 * @author rwinch
 */
public class ArgParser {
  private static Logger logger = Logger.getLogger(ArgParser.class);
  
  /**
   * A value of the argument or option
   * @author rwinch
   */
  public abstract static class Value {
    private String id;
    private String description;
    private boolean isRequired;
    private Object value;
    
    
    public Value(String id, String description,Object defaultValue) {
      this.id = id;
      this.description = description;
      this.value = defaultValue;
    }
    
    public Value(String id, String description) {
      this(id,description,null);
    }
    
    public String getId() {
      return id;
    }
    public String getDescription() {
      return description;
    }
    protected void setValue(Object value) {
      this.value = value;
    }
    protected Object getValue() {
      return value;
    }
    
    public int hashCode() {
      return id.hashCode();
    }
    public int compareTo(Object that) {
      Value thatValue = (Value) that;
      return getId().compareTo(thatValue.getId());
    }
    public boolean equals(Object that) {
      return compareTo(that) == 0;
    }
    public String toString() {
      return id+": "+value;
    }
  }
  /**
   * An argument 
   * 
   * @author rwinch
   */
  public static class Argument extends Value {
    public Argument(String id, String description,Object defaultValue) {
      super(id,description,defaultValue);
    }
    
    public Argument(String id, String description) {
      this(id,description,null);
    }
    
    public String get() {
      return (String)super.getValue();
    }
    public void set(String value) {
      super.setValue(value);
    }

  }
  /**
   * A Flag
   * 
   * @author rwinch
   */
  public static class Flag extends Value {
    public Flag(String id, String description,Boolean defaultValue) {
      super(id,description,defaultValue);
    }
    
    public Flag(String id, String description) {
      this(id,description,Boolean.FALSE);
    }
    
    public boolean isSet() {
      return get();
    }
    
    public boolean get() {
      Object v = super.getValue();
      if(logger.isDebugEnabled() && v!=null) {
        logger.debug("v="+v);
        logger.debug("v.class="+v.getClass());
      }
      return ((Boolean)v).booleanValue();
    }
    public void set(boolean value) {
      set(new Boolean(value));
    }
    public void set(Boolean value) {
      super.setValue(value);
    }

  }
  
  // the arguments for this argParser
  private HashMap arguments = new HashMap();
  // allows to use stdin for reading in values
  private BufferedReader stdin;
  
  public ArgParser() {
    stdin =  new BufferedReader(new InputStreamReader(System.in));
  }
  
  /**
   * Add an argument
   * @param id - the id of the argument to add
   * @param description - the description of this argument
   */
  public void addArgument(String id, String description) {
    arguments.put("-"+id,new Argument(id,description));
  }
  /**
   * Add a flag
   * @param id - the flag id
   * @param description - a description of the flag
   */
  public void addFlag(String id, String description) {
    arguments.put("-"+id,new Flag(id,description));
  }
  /**
   * Returns all the arguments
   * @return
   */
  public Map getArguments() {
    return arguments;
  }
  /**
   * Determines if a flag is set
   * @param id - the id of the flag to check
   * @return
   */
  public boolean isFlagSet(String id) {
    id = "-"+id;
    if(arguments.containsKey(id)) {
      logger.info("iscontained");
      Value v = (Value)arguments.get(id);
      if(v instanceof Flag) {
        logger.info("isflag");
        return ((Flag)v).isSet();
      }else {
        logger.info("isn't flag");
        return false;
      }
    }else {
      logger.info("not contained");
      return false;
    }
  }
  /**
   * Allows for help to be displayed
   * @return
   */
  public String getHelp() {
    StringBuffer result = new StringBuffer();
    Iterator iValues = arguments.values().iterator();
    while(iValues.hasNext()) {
      result.append(iValues.next());
      result.append(Utilities.EOL);
    }
    return result.toString();
  }
  /**
   * Get the string value of an argument
   * @param id - the id of the argument to get the value
   * @param defaultValue - a default value, if none is specified
   * @return
   */
  public String getString(String id, String defaultValue) {
    String result = null;
    try {
       result = getString(id);
    }catch (Exception exception) {}
    
    if(result == null) {
      return defaultValue;
    }else {
      return result;
    }
  }
  /**
   * Get the string value of an argument
   * @param id - the id of the argument to get the value   * 
   * @return
   */
  public String getString(String id) {
    id = "-"+id;
    if(arguments.containsKey(id)) {
      if(logger.isDebugEnabled()) {
        logger.debug(String.valueOf( ((Value)arguments.get(id)).getValue() ) );
      }
      return String.valueOf( ((Value)arguments.get(id)).getValue() );
    }else {
      throw new RuntimeException("Undefined value for id: "+id);
    }
    
  }
  /**
   * Return the argument as an integer
   * @param id - the id of the argument
   * @return
   */
  public int getInt(String id) {
    return Integer.parseInt(getString(id));
  }
  /**
   * Return the argument as an int
   * @param id - the id of the arugment
   * @param defaultValue - a default value
   * @return
   */
  public int getInteger(String id, int defaultValue) {
    return Integer.parseInt(getString(id,String.valueOf(defaultValue)));
  }
  public double getDouble(String id) {
    return Double.parseDouble(getString(id));
  }
  
  public double getDouble(String id, double defaultValue) {
    return Double.parseDouble(getString(id,String.valueOf(defaultValue)));
  }
  /**
   * Use this to prompt the user for values and put the values into the
   * argParser instance TODO: This method needs moved to
   * <code>AbstractApplication</code> because it doesn't really belong here.
   * 
   * @throws Exception
   */
  public void promptForValues() throws Exception {
    Iterator argNames = arguments.keySet().iterator();
    // for each argument
    while(argNames.hasNext()) {
      // get the argument
      String argName = (String)argNames.next();
      // get the value
      Value v = (Value)arguments.get(argName);
      // output the description
      System.out.print(v.getDescription()+": ");
      // read in the value
      String input = stdin.readLine();
      Object value;
      // if it is a flag
      if(v instanceof Flag) {
        logger.info("isFlag==true");
        if(input != null && !"".equals(input) && !"false".equalsIgnoreCase(input)) {
          logger.info("true");
          value = Boolean.TRUE;
        }else {
          logger.info("false");
          value = Boolean.FALSE;
        }
      }else {
        // else it is an argument
        value = input;
      }      
      // set the value
      v.setValue(value);
      logger.debug(v.getValue().getClass());
      if(logger.isDebugEnabled()) {
        logger.debug("value="+value);
        logger.debug("value.class="+value.getClass());
        logger.debug(argName+"="+this.getArguments().get(argName));
        logger.debug("isFlagSet="+this.isFlagSet(argName));
      }
    }
      
  }
  /**
   * Parse the arguments
   * @param args the arguments
   * @throws Exception
   */
  public void parse(String[] args) throws Exception {
    if(args == null) {
      return;
    }
    
    String id = null;
    // for each argument
    for(int i=0;i<args.length;i++) {
      if(logger.isDebugEnabled()) {
        logger.debug("args["+i+"]="+args[i]);
      }
      
      if(id == null) {
        // we are looking for an id
        id = args[i];
        Value v = (Value)arguments.get(id);
        if(v instanceof Flag) { 
          logger.info("isFlag");
          ((Flag)v).set(true);
          // looking for an id again (no value for flag)
          id = null;
        }else if(i+1>=args.length) {
          throw new Exception("Excpected value for Argument["+id+"]");         
        }
      }else {
        // we are looking for a value
        if(logger.isDebugEnabled()) {
          logger.debug("!isFlag: "+id);
        }
        // get the value
        Value v = (Value)arguments.get(id);
        if(logger.isDebugEnabled()) {
	        logger.debug("values: "+arguments);           
	        logger.debug("id="+args[i]);
        }
        // set the value
        ((Argument)v).set(args[i]);
        // we are looking for an id again
        id = null;
      }
    }
    if(logger.isDebugEnabled()) {
    	logger.debug("values="+arguments);
    }
  }
}
