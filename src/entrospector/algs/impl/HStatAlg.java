/*
 *
 */
package entrospector.algs.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.Utilities;
import entrospector.algs.interfaces.Algorithm;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;

/**
 * <p>
 * Gets the Hstat of a given graph
 * </p>
 * 
 * @author rwinch
 */
public class HStatAlg extends AbstractAlgorithm implements Algorithm {
  private static Logger logger = Logger.getLogger(HStatAlg.class);
  
  /**
   * <p>
   * This class isn't necessary now, but it allows for generality in that later
   * we may wish to add more to our result.
   * </p>
   * 
   * @author rwinch
   */
  public static class Result {
    private Double hStat;
    public Result(Double hStat) {
      this.hStat = hStat;
    }
    public Double getHStat() {
      return hStat;
    }
  }

  public Object run(Map args) {
    Graph graph = (Graph)args.get("graph");
    
    double result = 0;
    Collection vertices = graph.getVertices();
    Iterator iVertices = vertices.iterator();
    while(iVertices.hasNext()) {
      Vertex vertex = (Vertex) iVertices.next();
      HStatAlg.Result hStatResult = HStatAlg.getHStatForVertexIn(vertex,graph);
      Double hStat = hStatResult.getHStat();
      if(hStat != null) {
        result += hStat.doubleValue();
      }
    }    
    return new Result(new Double(result));
  }
  
  public static HStatAlg.Result getHStatForVertexIn(Vertex vertex, Graph graph) {
    int logBase = 2;
    int eCount = graph.getDegreeOf(vertex);
    float a = 1;

    if (eCount > 0) {
      Double result = new Double(-(Utilities.logOfBase(a / eCount, logBase)));
      if(logger.isDebugEnabled()) {
        logger.debug("eCount="+eCount+" result="+result);
      }
      return new HStatAlg.Result(result);
    } else {
      // can't divide by 0, and eCount should never be < 0
      if(logger.isDebugEnabled()) {
        logger.debug("eCount="+eCount+" result=null)");
      }
      return new HStatAlg.Result(null);
    }
  }

  /* (non-Javadoc)
   * @see entrospector.algs.interfaces.Algorithm#isInstance(java.lang.Object)
   */
  public boolean isInstance(Object o) {
    // no data always return false
    return false;
  }
  
  

}
