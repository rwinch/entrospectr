/*
 * 
 */
package entrospector.algs.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.algs.interfaces.Algorithm;
import entrospector.graph.impl.IDFactory;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Data;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;

/**
 * <p>
 * This algorithm does not actually perform duplications in the way a biologist
 * would envision it. Its goal is to create parameters for the Degeneration
 * algorithm. it is much faster to only add the connections that are needed
 * rather than add all and then remove what are not needed.
 * </p>
 * 
 * <p>
 * What the algorithm does
 * </p>
 * <ul>
 * <li>Returns a mapping of duplicated vertices to their duplicates (origionalToDuplicated)</li>
 * <li>Adds the duplicated vertices to the resulting graph</li>
 * <li>NOTE: NO Edges are added (see above for why)</li>
 * </ul>
 * 
 * @author rwinch
 */
public class DuplicationAlg extends AbstractAlgorithm {
  private static final Logger logger = Logger.getLogger(DuplicationAlg.class);
  
  private static Algorithm defaultAlg;
  
  public static class Result implements Data {
    private Graph graph;
    private Map origionalToDuplicated;
    public Result(Graph graph,Map origionalToDuplicated) {
      this.graph = graph;
      this.origionalToDuplicated = origionalToDuplicated;
    }
    public Graph getGraph() {
      return graph;
    }
    public Map getOrigionalToDuplicated() {
      return origionalToDuplicated;
    }
  }
  /**
   * Returns mapping of origionalVertice to a duplicatedVertice
   * from the origionalVertice
   * @param graph
   * @param duplicationCount
   * @return origionalVertice->duplicatedVertice
   */
  public Object run(Map args) {
    Graph graph = (Graph)args.get("graph");
    int duplicationCount = ((Integer)args.get("duplicationCount")).intValue();
    
    if(graph == null) {
      throw new IllegalArgumentException("Can't duplicate a null graph");
    }else if (graph.getVertices().size()<duplicationCount) {
      throw new IllegalArgumentException("duplicationCount ("+duplicationCount
          +") > graph.vertices ("+graph.getVertices().size()+")");
    }
    if(logger.isDebugEnabled()) {
      logger.debug("duplicationCount="+duplicationCount);
    }

    Graph g = new UndirectedGraphImpl();
    g.addEdges(graph.getEdges());     
    
    // {origionalVertice->duplicatedVertice}
    Map origionalToDuplicated = new HashMap();
    // init the id factory
    IDFactory idFactory =  new IDFactory(g.getVertices());    
    
    // duplicate count vertices
    Object[] vertices = getVertices(g);
    for(int i=0;i<duplicationCount;i++) {
      Vertex origionalVertice = (Vertex) vertices[i];      
      Vertex duplicatedVertice = (Vertex) origionalVertice.clone();
      duplicatedVertice.setIdentity(idFactory.next());
      
      // add to the graph
      g.addVertex(duplicatedVertice);
            
      // add the duplicatedVertice
      origionalToDuplicated.put(origionalVertice,duplicatedVertice);
    }  
    return new Result(g,origionalToDuplicated);
  }
  
  public Object[] getVertices(Graph g) {
    return RandomizeVerticesAlg.go(g);
  }

  /* (non-Javadoc)
   * @see entrospector.algs.interfaces.Algorithm#isInstance(java.lang.Object)
   */
  public boolean isInstance(Object o) {
    return o instanceof Result;
  }
  public static DuplicationAlg.Result go(Map args) {
      if(defaultAlg == null) {
        defaultAlg = new DuplicationAlg();
      }    
      synchronized (defaultAlg) {
        return (DuplicationAlg.Result)defaultAlg.run(args);
      }
  }
}
