/*
 * 
 */
package entrospector.algs.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.Utilities;
import entrospector.algs.interfaces.Algorithm;
import entrospector.graph.impl.UndirectedEdgeImpl;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Edge;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;

/**
 * <p>
 * This class will first degenerate guaranteeing that each orgional connection
 * is present, then adds <redundancyCnt>number of redundancy
 * </p>
 * 
 * @author rwinch
 */
public class DegenerationAlg extends AbstractAlgorithm {

  private static final Logger logger = Logger.getLogger(DegenerationAlg.class);
  
  public static final String PARAM_graph = "graph";
  public static final String PARAM_origionalToDuplicated = "orgionalToDuplicated";
  
  private static Algorithm defaultAlg;

  public static class Result {
    private Graph graph;
    private Collection redundantEdges;
    
    public Result(Graph graph,Collection redundantEdges) {
      this.graph = graph;
      this.redundantEdges = redundantEdges;
    }
    public Graph getGraph() {
      return graph;
    }
    public Collection getRedundantEdges() {
      return redundantEdges;
    }
  }
  
  // used for debug, keeps track of how many times we got a certain choice
  protected static int[] countCnt = new int[5];
  /* (non-Javadoc)
   * @see entrospector.algs.interfaces.Algorithm#run(java.util.Map)
   */
  public Object run(Map args) {
    Graph graph = (Graph)args.get(PARAM_graph);
    Map origionalToDuplicated = (Map)args.get(PARAM_origionalToDuplicated);
    if(logger.isDebugEnabled()) {
      logger.debug("orgionalToDuplicated.size="+origionalToDuplicated.size());
    }
    int redundancyCnt = 0;
    if(args.containsKey("redundancyCnt")) {
      redundancyCnt = ((Integer)args.get("redundancyCnt")).intValue();
    }
    
    // the graph for our result
    Graph g = new UndirectedGraphImpl();
    g.addEdges(graph.getEdges());
    
    // First make the graph non-redundant
    Collection redundantEdges = randomNonRedundant(graph,g,origionalToDuplicated);
    
    // if we want to add redundant edges
    if(redundancyCnt > 0) {
    	Object[] edges = redundantEdges.toArray();
    	// ranomize the redundantEdges
    	edges = Utilities.randomizeArray(edges,3);
    	// select the first <redundancyCnt> and add to the graph
    	for(int i=0;i<redundancyCnt;i++) {
    	  Edge redundantEdge = (Edge) edges[i];
    	  g.addEdge(redundantEdge);
    	}
    }
    if(logger.isDebugEnabled()) {
      logger.debug("choices="+arrayToString(countCnt));
    }
    countCnt = new int[5];
    return new Result(g,redundantEdges);    
  }
  /**
   * A debug method
   * @param a
   * @return
   */
  private static String arrayToString(int[] a) {
    StringBuffer result = new StringBuffer();
    result.append("[");
    for(int i=0;i<a.length;i++) {
      result.append(a[i]);
      result.append(",");
    }
    result.append("]");
    return result.toString();
  }
  
  /**
   * Makes the graph have non-redundant random edges and Returns a collection of
   * edges that could be added for redundancy
   * 
   * @param graph
   * @param g
   * @param origionalToDuplicated
   * @return
   */
  public Collection randomNonRedundant(Graph graph, Graph g, Map origionalToDuplicated) {
    // a list of edges that we may add for redundancy
    LinkedList redundantEdges = new LinkedList();
        
    // NOTE: the code below could be cleaned up to be shorter (combining cases),
    // but would make it difficult to read and not improve performance. Thus 
    // it is left as readable code.
    
    // iterate over the origional graph edges
    // this is because we can't modify a collection while
    // iterating over it and graph.getEdges() == g.getEdges()
    // for each edge see if it was duplicated, if it was modify it, else ignore
    Iterator iEdges = graph.getEdges().iterator();
    while(iEdges.hasNext()) {      
      // init
      Edge edge = (Edge)iEdges.next();
      Vertex vertexA = edge.getA();
      Vertex vertexB = edge.getB();
      if(logger.isDebugEnabled()) {
        logger.debug("processing edge="+edge);
      }
            
      // is self edge?
      if(vertexB.equals(vertexA)) {
        logger.info("self-edge");
        // was it duplicated?
        if(origionalToDuplicated.containsKey(vertexA)) {
          // get the duplicated vertex
          Vertex duplicateVertex = (Vertex) origionalToDuplicated.get(vertexA);
                    
          // it was have 3 choices
          int choice = nextChoice(3);
          if(logger.isDebugEnabled()) {
            logger.debug("choice="+choice);
          }
          
          // create our 3 possible edges
          Edge edge0 = edge; // the origional
          Edge edge1 = new UndirectedEdgeImpl(duplicateVertex,duplicateVertex);
          Edge edge2 = new UndirectedEdgeImpl(vertexA,duplicateVertex);
          
          if(choice == 0) {
            // dont remove origional
            // dont add new edge
            // add other choices to redundantEdges
            redundantEdges.add(edge1);
            redundantEdges.add(edge2);
          }else if(choice == 1) {
            // remove origional edge
            g.removeEdge(edge);
            // put the self edge on the duplicateVertex
            g.addEdge(edge1);
            // add other choices to redundantEdges
            redundantEdges.add(edge0);
            redundantEdges.add(edge2);
          }else if(choice == 2) {
            // remove the origional edge
            g.removeEdge(edge);
            // connect the origional and the duplicate
            g.addEdge(edge2);
            // add other choices to redundantEdges
            redundantEdges.add(edge0);
            redundantEdges.add(edge1);
          }else {
            throw new InternalError("Shouldn't get to this line. Choice should be in range (0,2)");
          }           
        }else {
          // it wasn't cloned
          // we don't need to do anything
        }        
      }else {
        logger.info("!selfEdge");
        // it wasn't a self edge
        boolean isACloned = origionalToDuplicated.containsKey(vertexA);
        boolean isBCloned = origionalToDuplicated.containsKey(vertexB);
        
        if(isACloned && isBCloned) {
          logger.info("both cloned");
          
          // both were cloned
          // 4 choices
          int choice = nextChoice(4);
          if(logger.isDebugEnabled()) {
            logger.debug("choice="+choice);
          }
          
          // get the duplicated vertices
          Vertex duplicateA = (Vertex)origionalToDuplicated.get(vertexA);
          Vertex duplicateB = (Vertex)origionalToDuplicated.get(vertexB);
                    
          // create our possible edges
          Edge edge0 = edge;
          Edge edge1 = new UndirectedEdgeImpl(vertexA,duplicateB);
          Edge edge2 = new UndirectedEdgeImpl(duplicateA,vertexB);
          Edge edge3 = new UndirectedEdgeImpl(duplicateA,duplicateB);
                    
          if(choice == 0) {
            // don't remove origional edge
            // don't add edge
            // add the rest to redundantEdges
            redundantEdges.add(edge1);
            redundantEdges.add(edge2);
            redundantEdges.add(edge3);
          }else if(choice == 1) {
            // remove origional edge
            g.removeEdge(edge);
            // add the edge
            g.addEdge(edge1);
            // add rest to redundantEdges
            redundantEdges.add(edge0);
            redundantEdges.add(edge2);
            redundantEdges.add(edge3);
          }else if(choice == 2) {
            // remove the origional edge
            g.removeEdge(edge);
            // add the edge            
            g.addEdge(edge2);
            // add the rest to redundantEdges
            redundantEdges.add(edge0);
            redundantEdges.add(edge1);
            redundantEdges.add(edge3);
          }else if(choice == 3) {
            // remove the origional edge
            g.removeEdge(edge);
            // add the edge
            g.addEdge(edge3);
            // add the rest to redundantEdges
            redundantEdges.add(edge0);
            redundantEdges.add(edge1);
            redundantEdges.add(edge2);
          }else {
            throw new InternalError("Shouldn't get to this line. Choice should be in range (0,3). Got "+choice);
          }
        }else if(isACloned && !isBCloned) {
          // only a is cloned
          logger.info("only a is cloned");
          
          // two choices
          int choice = nextChoice(2);
          if(logger.isDebugEnabled()) {
            logger.debug("choice="+choice);
          }
          
          // duplicated vertices
          Vertex duplicateA = (Vertex)origionalToDuplicated.get(vertexA);
          
          // our possible edges
          Edge edge0 = edge;
          Edge edge1 = new UndirectedEdgeImpl(duplicateA,vertexB);
          
          if(choice == 0) {
            // retain origional edge
            // don't add edge
            // add rest to redundantEdges
            redundantEdges.add(edge1);
          }else if(choice==1) {
            // remove orgional edge
            g.removeEdge(edge);
            // add the edge
            g.addEdge(edge1);
            // add rest to redundantEdges
            redundantEdges.add(edge0);
          }else {
            throw new InternalError("Shouldn't get here, there are no other possiblities");
          }          
        }else if(!isACloned && isBCloned) {
          // only b is cloned
          logger.info("only b is cloned");
          
          // two choices
          int choice = nextChoice(2);
          if(logger.isDebugEnabled()) {
            logger.debug("choice="+choice);
          }
          
          // duplicated vertices
          Vertex duplicateB = (Vertex)origionalToDuplicated.get(vertexB);
          
          // our possible edges
          Edge edge0 = edge;
          Edge edge1 = new UndirectedEdgeImpl(vertexA,duplicateB);
          
          if(choice == 0) {
            // do not remove orgional
            // do not add to graph
            // add rest to redundantEdges
            redundantEdges.add(edge1);
          }else if(choice == 1){
            // remove orgional edge
            g.removeEdge(edge);
            // add the edge to the graph            
            g.addEdge(edge1);
            // add rest to redundantEdges
            redundantEdges.add(edge0);
          }else {
            throw new InternalError("Shouldn't get here, there are no other possiblities");
          }
        }else if(!isACloned && !isBCloned) {
          if(logger.isDebugEnabled()) {
            logger.debug("neither cloned");
          }
          // neither were cloned
          // don't need to do anything
        }else {
          throw new InternalError("Shouldn't get here, there are no other possiblities");
        }
      }

      if(logger.isDebugEnabled()) {
        logger.debug("redundantEdges="+redundantEdges);
      }
    }
    return redundantEdges;
  }
  /**
   * Returns a value in range of 0 and choice - 1
   * @param choiceCnt
   * @return
   */
  public int nextChoice(int choiceCnt) {
    int result = Math.abs((Utilities.getRand().nextInt() % choiceCnt));
    countCnt[result]++;
    return result;
  }

  /* (non-Javadoc)
   * @see entrospector.algs.interfaces.Algorithm#isInstance(java.lang.Object)
   */
  public boolean isInstance(Object o) {
    // doens't have data w/ it, always return false
    return false;
  }
  /**
   * Singleton pattern, use a static instance to run the algrithm
   * @param args
   * @return
   */
  public static DegenerationAlg.Result go(Map args) {
    if(defaultAlg == null) {
      defaultAlg = new DegenerationAlg();
    }    
    synchronized (defaultAlg) {
      return (DegenerationAlg.Result)defaultAlg.run(args);
    }
  }


}
