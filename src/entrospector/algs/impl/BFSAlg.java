/*
 * 
 */
package entrospector.algs.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import entrospector.algs.interfaces.Algorithm;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Data;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;

/**
 * <p>
 * Performs a BFS on the given graph. Data is stored for each node for a
 * particular graph.
 * </p>
 * 
 * @author rwinch
 */
public class BFSAlg extends AbstractAlgorithm implements Algorithm {
  private static Logger logger = Logger.getLogger(BFSAlg.class);
  
  // some colors
  public final static Integer WHITE = new Integer(0), GRAY = new Integer(1),
      BLACK = new Integer(2);

  // our version of inifinity (nothing can be negative) in BFS
  public final static Integer INFINITY = new Integer(-1);

  /**
   * @author rwinch
   * 
   * Allows the BFS results to be seen
   */
  public class DataImpl implements Data {
    public final static int WHITE = 0, GREY = 1, BLACK = 2;

    /* this vertex and its parent */
    private Vertex v, p;

    // distance
    private int distance;

    // color
    private int color;

    /**
     * 
     * @param v
     *          this vertex
     * @param p
     *          parent of the vertex
     * @param distance
     *          the distance
     * @param color
     *          this vertices color
     */
    public DataImpl(Vertex v, Vertex p, int distance, int color) {
      this.v = v;
      this.p = p;
      this.distance = distance;
      this.color = color;
    }

    public Vertex getVertex() {
      return v;
    }

    public Vertex getParent() {
      return p;
    }

    public int getDistance() {
      return distance;
    }

    public int getColor() {
      return color;
    }

    public String toString() {
      return "(" + ( (v != null) ? v.getIdentity() : null )+ " " + ( (p != null)?  p.getIdentity() : null ) + " " + distance + " " + color + ")";
    }
  }


  /**
   * Returns either a collection of BFSWrapper's or a Collection of Collections of BFSWrapper's
   * Each collection of BFSWrapper's is a subnetworks 
   */
  public Object run(Map args) {
    Graph graph = (Graph)args.get("graph");
    
    Graph g = (Graph)graph.clone();
    
    Object result = null;
    
    if(args.containsKey("source")) {
      Vertex source = (Vertex)args.get("source");
      result = bfs( g, source);
    }else {
      result = bfs(g);
    }
    return result;
  }
  /**
   * Returns a collection of connected graphs
   * @param graph
   * @return
   */
  public Collection bfs(Graph graph) {
    Collection result = new ArrayList();
    Graph g = new UndirectedGraphImpl();
    g.addGraph(graph);
    
    while(!g.getVertices().isEmpty()) {
      Vertex source = (Vertex) g.getVertices().iterator().next();
      Graph subGraph = bfs(g,source);
      result.add(subGraph);
      g.removeGraph(subGraph);
    }
    
    return result;
  }

  /**
   * Returns a Graph that is connected to source
   * 
   * @param graph -
   *          the graph to read for source
   * @param source -
   *          the source node, it is connected to all verticies for input graph
   *          in the returned graph
   * @return
   */
  public Graph bfs(Graph graph, Vertex source) {
    if(logger.isDebugEnabled()) {
      logger.debug("bfs ("+graph+", "+source+")");
    }
    Graph result = new UndirectedGraphImpl();
    
    // d = distances, p = parents, c = colors
    Map d = new TreeMap(), p = new TreeMap(), color = new TreeMap();

    /* initialize */
    Iterator iVertices = graph.getVertices().iterator();
    while (iVertices.hasNext()) {
      Object v = iVertices.next();
      d.put(v, INFINITY);
      p.put(v, null);
      color.put(v, WHITE);
    }

    /* change the values of our source */
    d.put(source, new Integer(0));
    color.put(source, GRAY);

    // Create a queue
    List Q = new LinkedList();
    Q.add(source);

    // while we have something in the Q
    while (!Q.isEmpty()) {
      // extract the first value
      Vertex u = (Vertex) Q.remove(0);

      if(logger.isDebugEnabled()) {
        logger.debug("Extracted: "+u);
      }
      
      // our adjacency list
      Iterator iAdj = graph.getNeighborsOf(u).iterator();
      // the distance
      Integer distance = new Integer(((Integer) d.get(u)).intValue() + 1);
      // for each vertex in the adjacency list
      while (iAdj.hasNext()) {
        // get the neighbor
        Vertex v = (Vertex)iAdj.next();      
        
        // if it is white
        if (WHITE.equals(color.get(v))) {          
          // set the distance
          d.put(v, distance);
          // set the parent of v to u
          p.put(v, u);
          // set the color to GRAY
          color.put(v, GRAY);
          // add v to the end of our Q
          Q.add(v);
        }
      }// end of Adj loop
      // set the color to black
      color.put(u, BLACK);
    }// end Q loop

    // Now wrap using our wrapper object
    // This way we do not need to have a hash set (which takes up more space)
    // to see values
    iVertices = graph.getVertices().iterator();
    // for each vertex
    while (iVertices.hasNext()) {
      Vertex v = (Vertex) iVertices.next();
      Vertex parent = (Vertex) p.get(v);
      Integer dist = (Integer) d.get(v);
      int distance = (dist).intValue();
      int c = ((Integer) color.get(v)).intValue();
      // add the values to result
      if (!INFINITY.equals(d.get(v))) {
        DataImpl data = new DataImpl(v, parent, distance, c);
        Vertex newVertex = (Vertex)v.clone();        
        result.addVertex(newVertex);
        result.addDataFor(data,newVertex);
      }
    }
    iVertices = result.getVertices().iterator();
    while (iVertices.hasNext()) {
      Vertex v = (Vertex) iVertices.next();
      result.addEdges(graph.getSelfEdgesOf(v));
      result.addEdges(graph.getSourceEdgesOf(v));  
    }
    
    return result;
  }
  
  public boolean isInstance(Object obj) {
    return obj instanceof BFSAlg.DataImpl;
  }
}
