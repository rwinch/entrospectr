/*
 * 
 */
package entrospector.algs.impl;

import java.util.Map;

import entrospector.algs.interfaces.Algorithm;

/**
 * <p>
 * Returns a colleciton of subnetworks as a collection of graphs using BFSAlg
 * </p>
 * 
 * @author rwinch
 */
public class ConnectedSubNetworks extends BFSAlg implements Algorithm {
  /**
   * Returns a collection of subnetworks as a collection of graphs
   */
  public Object run(Map args) {
    return new BFSAlg().run(args);
  }

}
