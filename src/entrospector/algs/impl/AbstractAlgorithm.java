/*
 * 
 */
package entrospector.algs.impl;

import java.util.Collection;
import java.util.Iterator;

import entrospector.algs.interfaces.Algorithm;
import entrospector.graph.interfaces.Data;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;

/**
 * @author rwinch
 */
public abstract class AbstractAlgorithm implements Algorithm {
  
  public boolean stripGraphData(Graph graph) {
    boolean result = false;
    
    if(graph == null)
      return result;
    
    Collection vertices = graph.getVertices();
    if(vertices == null) {
      return result;
    }else {
      Iterator iVertices = vertices.iterator();
      while(iVertices.hasNext()) {
        Vertex v = (Vertex)iVertices.next();
        boolean vResult = stripVertexData(v);
        if(vResult) {
          result = true;
        }
      }
    }
    return result;
  }
  
  public boolean stripVertexData(Vertex v) {
    boolean result = false;
    
    if(v == null)
      return result;
    
    Collection data = v.getData();
    if(data == null) {
      return result;
    }else {
      Iterator iData = data.iterator();
      while(iData.hasNext()) {
        Data d = (Data)iData.next();
        if(isInstance(d)) {
          iData.remove();
          result = true;
        }
      }
    }
    
    return result;    
  }

}
