/*
 * 
 */
package entrospector.algs.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.algs.interfaces.Algorithm;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;

/**
 * <p>
 * Randomly deletes a given number of verticies and the edges associated with
 * those vertices
 * </p>
 * 
 * @author rwinch
 */
public class RandomDeletionsAlg extends AbstractAlgorithm {
  private static final Logger logger = Logger.getLogger(RandomDeletionsAlg.class);
  
  private static Algorithm defaultAlg; 
  
  public static class Result {
    private Graph graph;
    
    public Result(Graph graph) {
      this.graph = graph;
    }
    
    public Graph getGraph() {
      return this.graph;
    }
  }
  
  /* (non-Javadoc)
   * @see entrospector.algs.interfaces.Algorithm#run(java.util.Map)
   */
  public Object run(Map args) {
    Graph graph = (Graph)args.get("graph");
    int deletionCount = ((Integer)args.get("deletionCount")).intValue();
    
    // init origionalHStat
    Map origionalHStat = null;
    // optionally get origionalHStat
    if(args.containsKey("origionalHStat")) {
      origionalHStat = (Map)args.get("origionalHStat");
    }else {
      origionalHStat = getOrigionalHStat(graph);
    }
    if(logger.isDebugEnabled()){
      logger.debug("peformDeletions ("+deletionCount+")");
    }
    
    Object[] vertices = getVerticies(graph);
    
    // now copy our graph, so we don't need to read it in again
    Graph g = new UndirectedGraphImpl();
    g.addGraph(graph);
    if(logger.isDebugEnabled()) {
      logger.debug("peforming "+deletionCount+" deletions");
    }
    for(int i=0;i<deletionCount;i++) {
      Vertex vToRemove = (Vertex)vertices[i];
      g.removeVertex(vToRemove);
    }    

    return new RandomDeletionsAlg.Result(g);
  }
  /**
   * This is a separate method so that we can extend this class
   * and override the method to test deletions.
   * @param graph
   * @return
   */
  public Object[] getVerticies(Graph graph) {
    return RandomizeVerticesAlg.go(graph);
  }  
    
  public static Map getOrigionalHStat(Graph graph) {
    Map result = new HashMap();
    //  initialize origionalHStat we use this to compare each vertex values
    // after a deletion is done to see if the hStat value has changed
    logger.info("initializing hStat values...");
    Iterator iVertices = graph.getVertices().iterator();
    while(iVertices.hasNext()) {
      Vertex v = (Vertex)iVertices.next();
      Double d = HStatAlg.getHStatForVertexIn(v,graph).getHStat();
      result.put(v,d);
    }
    return result;
  }
  
  public static RandomDeletionsAlg.Result go(Map args) {
    if(defaultAlg == null) {
      defaultAlg = new RandomDeletionsAlg();
    }    
    synchronized (defaultAlg) {
      return (RandomDeletionsAlg.Result)defaultAlg.run(args);
    }
  }


  /* (non-Javadoc)
   * @see entrospector.algs.interfaces.Algorithm#isInstance(java.lang.Object)
   */
  public boolean isInstance(Object o) {
    // TODO Auto-generated method stub
    return false;
  }

}
