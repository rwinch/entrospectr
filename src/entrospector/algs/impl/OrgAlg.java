/*
 * 
 */
package entrospector.algs.impl;

import java.util.Map;

import entrospector.algs.interfaces.Algorithm;

/**
 * <p>
 * Gets the Org of a given network. This Alg is not very useful since we can do
 * this easily using the HFlat Alg.
 * </p>
 * 
 * @author rwinch
 */
public class OrgAlg extends AbstractAlgorithm {

  /**
   * This could be optimized for speed at a later time, right now it is optimized
   * for ease of use, later optimize for speed
   */
  public Object run(Map args) {
    Algorithm hFlat = new HFlatAlg();
    
    HFlatAlg.Result hFlatResult = (HFlatAlg.Result)hFlat.run(args);
    Double hFlatValue = hFlatResult.getHFlat();
    Double hStatValue = hFlatResult.getHStat();
        
    if(hStatValue.doubleValue() != 0) {      
      return new Double(hFlatValue.doubleValue() / hStatValue.doubleValue()); 
    }else {
      return null;
    }    
  }

  /* (non-Javadoc)
   * @see entrospector.algs.interfaces.Algorithm#isInstance(java.lang.Object)
   */
  public boolean isInstance(Object o) {
    // never has data in the nodes
    return false;
  }

}
