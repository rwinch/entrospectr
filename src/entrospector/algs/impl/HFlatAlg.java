/*
 * 
 */
package entrospector.algs.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.Utilities;
import entrospector.algs.interfaces.Algorithm;
import entrospector.graph.impl.UndirectedGraphImpl;
import entrospector.graph.interfaces.Graph;

/**
 * <p>
 * Gets the HFlat of a network. Note that certain other values (i.e. HStat) are
 * computed in order to get HFlat, so those values are included in the returned
 * result as well.
 * </p>
 * 
 * @author rwinch
 */
public class HFlatAlg extends AbstractAlgorithm implements Algorithm {
  private Logger logger = Logger.getLogger(HFlatAlg.class);
  
  private static Algorithm defaultAlg;
  
  private ConnectedSubNetworks connectedNetworks;
  private HStatAlg hStatAlg; 
  
  private void reset() {
    connectedNetworks = new ConnectedSubNetworks();
    hStatAlg = new HStatAlg();
  }
  
  /**
   * Returns the HFlat value of a graph
   */
  public Object run(Map args) {
    reset();
    
    Graph graph = (Graph)args.get("graph");

    Double hFlat = null;
    
    Graph g = new UndirectedGraphImpl();
    Result result = filterGraph(graph);
    g.addGraphs(result.getRetainedGraphs());
    
    // the vertice count
    double vCount = g.getVertices().size();
    if(logger.isDebugEnabled()) {
      logger.debug("vCount="+vCount);
    }
    // our log base
    int logBase = 2;

    // if the vertice count is 0, cannot dived by 0 so return null
    if (vCount == 0) {      
      hFlat = null;
    } else {
      // TODO: We most likely want the selfEdgeCnt + 2*(edgeCnt - selfEdgeCnt)
      // Otherwise we will not get an accurate average      
      // get an edge count      
      int selfEdgeCnt = g.getSelfEdges().size();
      int edgeCnt = g.getEdges().size();
      double eCount = selfEdgeCnt + 2*(edgeCnt - selfEdgeCnt);
      if(logger.isDebugEnabled()) {
        logger.debug("eCount="+eCount);
      }

      // hFlat is null if eCount is 0 since can't divide by 0
      if (eCount == 0) {
        hFlat = null;
      } else {
        // otherwise return the answer
        hFlat = new Double( (vCount
            * Utilities.logOfBase( (eCount) / vCount, logBase) ) );
      }
    }
    
    result.setHFlat(hFlat);
    
    return result;
  }
  
  public class Result {
    private Collection retained, filtered;
    private Double hStat = null;
    private Double hFlat = null;
    
    public Result (Collection retained, Collection filtered, Double hStat) {
      this.retained = retained; this.filtered = filtered; this.hStat = hStat;
    }
    public Collection getRetainedGraphs() {
      return retained;
    }
    public Collection getFilteredGraphs() {
      return filtered;
    }
    public Double getHStat() {
      return hStat;
    }
    public Double getHFlat() {
      return hFlat;
    }
    public void setHFlat(Double d) {
      hFlat = d;
    }
  }
  
  /**
   * Given a graph will remove all vertices that belong to networks with HStat <= 0
   * @param graph
   * @return
   */
  public Result filterGraph(Graph graph) {
    if(connectedNetworks == null) {
      connectedNetworks = new ConnectedSubNetworks();
    }
    if(hStatAlg == null) {
      hStatAlg = new HStatAlg();
    }
    Collection retained = new LinkedList();
    Collection filtered = new LinkedList();    
    double hStat = 0;     
    
    Collection graphs = (Collection)connectedNetworks.run(
        Collections.singletonMap("graph",graph));    
    
    Iterator iGraphs = graphs.iterator();
    while(iGraphs.hasNext()) {
      Graph g = (Graph)iGraphs.next();
      HStatAlg.Result hStatResult = (HStatAlg.Result) hStatAlg.run(Collections.singletonMap("graph",g));
      Double d = hStatResult.getHStat();      
      if(d != null && d.doubleValue() > 0) {
        retained.add(g);
        hStat += d.doubleValue();
        
        if(logger.isDebugEnabled()) {
          logger.debug("adding graph: "+d+", "+g.getEdges());
        }
      }else {
        if(logger.isDebugEnabled()) {
          logger.debug("skipping graph: "+d+", "+g.getEdges());
        }
        filtered.add(g);
      }
    }    
    
    return new Result(retained,filtered,new Double(hStat));
  }

  /* (non-Javadoc)
   * @see entrospector.algs.interfaces.Algorithm#isInstance(java.lang.Object)
   */
  public boolean isInstance(Object o) {
    // there is no data saved in a graph or vertex for HFlat
    return false;
  }
  
  public static HFlatAlg.Result go(Map args) {
    if(defaultAlg == null) {
      defaultAlg = new HFlatAlg();
    }    
    synchronized (defaultAlg) {
      return (HFlatAlg.Result)defaultAlg.run(args);
    }
  }


}
