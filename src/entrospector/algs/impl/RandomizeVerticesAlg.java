/*
 * 
 */
package entrospector.algs.impl;

import java.util.Collections;
import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.Utilities;
import entrospector.algs.interfaces.Algorithm;
import entrospector.graph.interfaces.Graph;


/**
 * <p>
 * Randomizes the vertices in a graph
 * </p>
 * 
 * @author rwinch
 */
public class RandomizeVerticesAlg extends AbstractAlgorithm {
  private static Logger logger = Logger.getLogger(RandomizeVerticesAlg.class);
  
  private static Algorithm defaultAlg;

  /* (non-Javadoc)
   * @see entrospector.algs.interfaces.Algorithm#run(java.util.Map)
   */
  public Object run(Map args) {
    Graph graph = (Graph)args.get("graph");
    
    // strength*vertices.length is the number of swaps
    int strength = 3;
    if(args.containsKey("strength")) {
      strength = ((Integer)args.get("strength")).intValue();
    }
    
    Object[] vertices = graph.getVertices().toArray();
        
    return Utilities.randomizeArray(vertices,strength);
  }
  
  public static Object[] go(Graph graph) {
    if(defaultAlg == null) {
      defaultAlg = new RandomizeVerticesAlg();
    }    
    synchronized (defaultAlg) {
      return (Object[])defaultAlg.run(Collections.singletonMap("graph",graph));
    }
  }

  /* (non-Javadoc)
   * @see entrospector.algs.interfaces.Algorithm#isInstance(java.lang.Object)
   */
  public boolean isInstance(Object o) {
    // TODO Auto-generated method stub
    return false;
  }

}
