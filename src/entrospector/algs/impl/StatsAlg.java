/*
 * 
 */
package entrospector.algs.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import entrospector.Utilities;
import entrospector.algs.interfaces.Algorithm;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;

/**
 * <p>
 * Collects the statistics from an origional graph and a modified graph
 * </p>
 * 
 * @author rwinch
 */
public class StatsAlg extends AbstractAlgorithm {
  private static final Logger logger = Logger.getLogger(StatsAlg.class);
  private static final String DELIMITER = ",";
  
  private static Algorithm defaultAlg;
  
  public static class Result {
    private HFlatAlg.Result hFlatResult;
    private double vDeltaHStatPercent;
    private int vDeltaHStatCnt;
    public Result(HFlatAlg.Result hFlatResult,int vDeltaHStatCnt,double vDeltaHStatPercent) {      
      this.hFlatResult = hFlatResult;
      this.vDeltaHStatCnt = vDeltaHStatCnt;
      this.vDeltaHStatPercent = vDeltaHStatPercent;
    }
    /**
     * A result of running HFlatAlg
     * @return
     */
    public HFlatAlg.Result getHFlatResult() {
      return hFlatResult;
    }
    /**
     * Returns the number of vertices that changed
     * @return
     */
    public int getVDeltaHStatCnt() {
      return this.vDeltaHStatCnt;
    }
    /**
     * The percent of vertices that hStat changed for
     * @return
     */
    public double getVDeltaHStatPercent() {
      return vDeltaHStatPercent;
    }
    public static String getHeader() {
      return "# HSTAT,HFLAT,NETWORK_COUNT,NETWORKS_FILTEREDOUT_COUNT,%_VERTICES_THAT_HSTAT_CHANGED"+Utilities.EOL+Utilities.EOL;
    }
    /**
     * The number of networks
     * @return
     */
    public int getNetworkCnt() {
      return getHFlatResult().getFilteredGraphs().size() + getHFlatResult().getRetainedGraphs().size();
    }
    public String toString() {
      StringBuffer result = new StringBuffer();
      result.append(getHFlatResult().getHStat());
      result.append(DELIMITER);
      result.append(getHFlatResult().getHFlat());
      result.append(DELIMITER);
      result.append(getNetworkCnt());
      result.append(DELIMITER);
      result.append(getHFlatResult().getFilteredGraphs().size());
      result.append(DELIMITER);
      result.append(getVDeltaHStatPercent());
      result.append(DELIMITER);
      result.append(Utilities.EOL);
      
      return result.toString();
    }
  }

  /* (non-Javadoc)
   * @see entrospector.algs.interfaces.Algorithm#run(java.util.Map)
   */
  public Object run(Map args) {
    logger.info("run");
    // init arguments
    Graph graph = (Graph)args.get("newGraph");
    
    Graph g = (Graph)graph.clone();
    
    //  init origionalHStat
    Map origionalHStat = null;
    // was it a parameter?
    if(args.containsKey("origionalHStat")) {
      origionalHStat = (Map)args.get("origionalHStat");
    }else {
      Graph origionalGraph = (Graph) args.get("origionalGraph");
      origionalHStat = getOrigionalHStat(origionalGraph);
    }
        
    int vDeltaHStatCnt = getVDeltaHStat(graph,origionalHStat);
    if(logger.isDebugEnabled()) {
      logger.debug("vDeltaHStatCnt="+vDeltaHStatCnt);
    }
    double total = g.getVertices().size();
    double vDeltaHStatPercent = vDeltaHStatCnt / total;
    
    HFlatAlg.Result hFlatResult = HFlatAlg.go(
        Utilities.arrayToMap(
            new Object[] {"graph",g}));
    
    
    return new Result(hFlatResult,vDeltaHStatCnt,vDeltaHStatPercent);
  }
  
  public static int getVDeltaHStat(Graph g, Map origionalHStat) {
    logger.info("getVDeltaHStat");
    
    // number of verticies that changed hStat
    int vDeltaHStatCnt = 0;
    Iterator iVertices = g.getVertices().iterator();
    while (iVertices.hasNext()) {
      Vertex v = (Vertex) iVertices.next();
      if(logger.isDebugEnabled()) {
        logger.debug("checking vertex "+v);
      }
      
      Double newHStat = HStatAlg.getHStatForVertexIn(v,g).getHStat();
      Double oldHStat = (Double)origionalHStat.get(v);
      
      if ( oldHStat != null && newHStat != null && oldHStat.doubleValue() != newHStat.doubleValue() ) {        
        vDeltaHStatCnt++;
      }      
      if(logger.isDebugEnabled()) {
        logger.debug("is it in origionalHStatKeys? "+origionalHStat.keySet().contains(v));
        logger.debug(v+"=> '"+oldHStat+"', '"+newHStat+"'");
        logger.debug("vDelataHStatCnt="+vDeltaHStatCnt);
      }
    }
    logger.debug("done checking");
        
    return vDeltaHStatCnt;
  }
  
  public static Map getOrigionalHStat(Graph graph) {
    Map result = new HashMap();
    //  initialize origionalHStat we use this to compare each vertex values
    // after a deletion is done to see if the hStat value has changed
    if(logger.isDebugEnabled()) {
      logger.debug("initializing hStat values...");
    }
    Iterator iVertices = graph.getVertices().iterator();
    while(iVertices.hasNext()) {
      Vertex v = (Vertex)iVertices.next();
      Double d = HStatAlg.getHStatForVertexIn(v,graph).getHStat();
      result.put(v,d);
    }
    return result;
  }  

  /* (non-Javadoc)
   * @see entrospector.algs.interfaces.Algorithm#isInstance(java.lang.Object)
   */
  public boolean isInstance(Object o) {
    return false;
  }
  
  public static StatsAlg.Result go(Map args) {
    if(defaultAlg == null) {
      defaultAlg = new StatsAlg();
    }    
    synchronized (defaultAlg) {
      return (StatsAlg.Result)defaultAlg.run(args);
    }    
  }

}
