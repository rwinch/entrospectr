/*
 * Created on Feb 18, 2005
 */
package entrospector.algs.interfaces;

import java.util.Map;

import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Vertex;

/**
 * <p>
 * The basic interface for an algorithm
 * </p>
 * 
 * @author rwinch
 */
public interface Algorithm {
  /**
   * Run this algorithm
   * @param args
   * @return
   */
  Object run(Map args); 
  /**
   * Removes data added to a vertex by this algorithm
   * @param v
   * @return
   */
  boolean stripVertexData(Vertex v);
  /**
   * Removes data added to a graph by this algorithm
   * @param g
   * @return
   */
  boolean stripGraphData(Graph g);
  /**
   * Determiens if the data is an instance of the data returned by this
   * algorithm
   * 
   * @param o
   * @return
   */
  boolean isInstance(Object o);
}
