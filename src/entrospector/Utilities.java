/*
 *
 */
package entrospector;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;

/**
 * @author rwinch
 */
public class Utilities {
  private static final Logger logger = Logger.getLogger(Utilities.class);

  public static String TEST_DATA_DIRECTORY = EntrospectorProperties
      .getDefault().getProperty("entrospector.testdatadir");

  public static String EOL = System.getProperty("line.separator");

  private static final Random rand = new Random(System.currentTimeMillis());

  /**
   * Use this random timer because things are not random in a computer. If you
   * are always recreating the random number generator you will see patterns.
   * 
   * @return
   */
  public static Random getRand() {
    synchronized (rand) {
      return rand;
    }
  }

  /**
   * Randomizes an array by peforming array.length*strength random swaps on the
   * array
   * 
   * @param array
   * @param strength
   * @return
   */
  public static Object[] randomizeArray(Object[] array, int strength) {
    //  randomize our array
    logger.info("randomize an array...");
    for (int i = 0; i < array.length * strength; i++) {
      int a = Utilities.getRand().nextInt() % array.length;
      int b = Utilities.getRand().nextInt() % array.length;
      // ensure that each is non-negative
      a = (a > 0) ? a : -a;
      b = (b > 0) ? b : -b;

      // perform a swap
      Object tmp = array[a];
      array[a] = array[b];
      array[b] = tmp;
    }
    return array;
  }

  /**
   * Nice since Java does have log base of
   * 
   * @param a
   * @param b
   * @return
   */
  public static double logOfBase(double a, double b) {
    return Math.log(a) / Math.log(b);
  }

  /**
   * Turn an array to a Map
   * 
   * @param array
   * @return
   */
  public static Map arrayToMap(Object[] array) {
    Map result = new HashMap();
    if (array == null) {
      return null;
    } else if (array.length % 2 != 0) {
      throw new IllegalArgumentException(
          "Array must contain an even number of elements");
    }

    Object key = null;
    for (int i = 0; i < array.length; i++) {
      if (i % 2 == 0) {
        key = array[i];
      } else {
        result.put(key, array[i]);
        key = null;
      }
    }

    return result;

  }

}