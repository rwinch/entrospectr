/*
 * 
 */
package entrospector.graph.impl;

import entrospector.graph.interfaces.Identity;

/**
 * @author rwinch
 */
public abstract class AbstractIdentity implements Identity {
  private Comparable identity;
  
  public AbstractIdentity(Comparable identity) {
    this.identity = identity;
  }
  
  public Comparable getIdentity() {
    return identity;
  }
    
  public boolean equals(Object that) {
    return this.compareTo(that) == 0;
  }
  public int hashCode() {
    return identity.hashCode();
  }
  public String toString() {
    return String.valueOf(identity);
  }
}
