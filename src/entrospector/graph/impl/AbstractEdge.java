/*
 *
 */
package entrospector.graph.impl;

import entrospector.graph.interfaces.Edge;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Identity;
import entrospector.graph.interfaces.Vertex;

/**
 * @author rwinch
 */
public abstract class AbstractEdge 
  extends AbstractGraphElementImpl implements Edge {
  
  private Vertex edgeA, edgeB;
  
  public AbstractEdge(Vertex edgeA, Vertex edgeB, Identity identity) {
    super(identity);
    
    this.edgeA = edgeA;
    this.edgeB = edgeB;
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Edge#getDestination()
   */
  public Vertex getA() {
    return edgeA;
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Edge#getSource()
   */
  public Vertex getB() {
    return edgeB;
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.GraphElement#isPresentIn(entrospector.graph.interfaces.Graph)
   */
  public boolean isPresentIn(Graph g) {
    return g.isEdgePresent(this);
  }
}
