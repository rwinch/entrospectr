/*
 *
 */
package entrospector.graph.impl;

import java.util.Collection;
import java.util.LinkedList;

import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.GraphElement;
import entrospector.graph.interfaces.Identity;

/**
 * @author rwinch
 */
public abstract class AbstractGraphElementImpl 
  	extends AbstractGraphObjectImpl  implements GraphElement {
  
  private Collection graphs;
  
  public AbstractGraphElementImpl(Identity identity) {
    super(identity);
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.GraphElement#getGraphs()
   */
  public Collection getGraphs() {    
    return graphs;
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.GraphElement#addGraph(entrospector.graph.interfaces.Graph)
   */
  public void addGraph(Graph g) {
    if(graphs == null) {
      graphs = new LinkedList();
    }
    graphs.add(g);
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.GraphElement#removeGraph(entrospector.graph.interfaces.Graph)
   */
  public boolean removeGraph(Graph g) {
    return graphs.remove(g);
  }
}
