/*
 *
 */
package entrospector.graph.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.TreeSet;

import entrospector.graph.interfaces.GraphObject;
import entrospector.graph.interfaces.Identity;

/**
 * @author rwinch
 */
public class IDFactory {
  TreeSet ids = new TreeSet();
  
  public IDFactory(Collection graphObjects) {
    if(graphObjects != null) {
      Iterator iGraphComponenets = graphObjects.iterator();
      while(iGraphComponenets.hasNext()) {
        GraphObject graphObject = (GraphObject)iGraphComponenets.next();
        ids.add(graphObject.getIdentity());
      }
    }
  }
  
  public IDFactory(TreeSet ids) {
    this.ids = ids; 
  }
  /**
   * Adds and returns the next available identity
   */
  public Identity next() {
    Identity result = last().next();
    ids.add(result);
    return result;
  }  
  public Identity first() {
    return (Identity)ids.first();
  }
  public Identity last() {
    return (Identity)ids.last();
  }
  
  public static Identity create(String identity) {
    Identity result = null;
    try {
      result = new IntegerIdentity(identity);
    }catch (Exception e) {
      result = new StringIdentity(identity);
    }
    return result;
  }
}
