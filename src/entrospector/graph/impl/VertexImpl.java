/*
 *
 */
package entrospector.graph.impl;

import org.apache.log4j.Logger;

import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.GraphObject;
import entrospector.graph.interfaces.Identity;
import entrospector.graph.interfaces.Vertex;

/**
 * @author rwinch
 */
public class VertexImpl 
  	extends AbstractGraphElementImpl implements Vertex {
  private static final Logger logger = Logger.getLogger(VertexImpl.class);
  public VertexImpl(Identity identity) {
    super(identity);
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.GraphElement#isPresentIn(entrospector.graph.interfaces.Graph)
   */
  public boolean isPresentIn(Graph g) {
    return g.isVertexPresent(this);
  }
 
  /* (non-Javadoc)
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  public int compareTo(Object that) {
    if(logger.isDebugEnabled()) {
      logger.debug("compareTo("+that+")");
    }
    if(that instanceof GraphObject) {
      return getIdentity().compareTo(((GraphObject)that).getIdentity());
    }else {
      return getIdentity().compareTo(that);
    }
  }
  public static VertexImpl create(String id) {
    return new VertexImpl(IDFactory.create(id));
  }
  public String toString() {
    return String.valueOf(this.getIdentity());
  }

}
