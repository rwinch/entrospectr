/*
 * Created on Feb 13, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package entrospector.graph.impl;

import org.apache.log4j.Logger;

import entrospector.graph.interfaces.Edge;
import entrospector.graph.interfaces.Identity;
import entrospector.graph.interfaces.Vertex;

/**
 * @author rwinch
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class UndirectedEdgeImpl extends AbstractEdge {
  private static Logger logger = Logger.getLogger(UndirectedEdgeImpl.class);
  public UndirectedEdgeImpl(Vertex source, Vertex destination) {
    this(source,destination,null);
  }
  public UndirectedEdgeImpl(Vertex source, Vertex destination, Identity identity) {
    super(source,destination,identity);
  }  
  
  /* (non-Javadoc)
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  public int compareTo(Object that) {
    if(logger.isDebugEnabled()) {
      logger.debug(this+".compareTo("+that+") ?");
    }
    
    Edge thatEdge = (Edge) that;    
    int result = 0;
    
    Vertex thisLarger, thisSmaller;
    Vertex thatLarger, thatSmaller;
    
    // find larger/smaller of this
    if(this.getB().compareTo(this.getA()) > 0){
      thisLarger = this.getB();
      thisSmaller = this.getA();
    }else {
      thisLarger = this.getA();
      thisSmaller = this.getB();
    }
    
    //  find larger/smaller of that
    if(thatEdge.getB().compareTo(thatEdge.getA()) > 0){
      thatLarger = thatEdge.getB();
      thatSmaller = thatEdge.getA();
    }else {
      thatLarger =  thatEdge.getA();
      thatSmaller = thatEdge.getB();
    }
    
    if(logger.isDebugEnabled()) {
      logger.debug("thisLarger "+thisLarger + " thisSmaller "+thisSmaller);
      logger.debug("thatLarger "+thatLarger + " thatSmaller "+thatSmaller);
    }
    // compare the two larger
    int compareLarger = thisLarger.compareTo(thatLarger);
    if(compareLarger != 0) {
      result = compareLarger;
    }else {
      // compare the two smaller
      int compareSmaller = thisSmaller.compareTo(thatSmaller);
      if(compareSmaller != 0) {
        result = compareSmaller;
      }else {
        result = 0;
      }
    }
    if(logger.isDebugEnabled()) {
      logger.debug(this+".compareTo("+that+") = "+result);
    }
    return result;
  }
  public static UndirectedEdgeImpl create(String a, String b) {
    return new UndirectedEdgeImpl(VertexImpl.create(a),VertexImpl.create(b));
  }
  
  public String toString() {
    return "( "+getA() + ", "+getB()+" )";
  }
}
