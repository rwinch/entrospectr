/*
 *
 */
package entrospector.graph.impl;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import entrospector.graph.interfaces.Data;
import entrospector.graph.interfaces.GraphObject;
import entrospector.graph.interfaces.Identity;

/**
 * @author rwinch
 */
public abstract class AbstractGraphObjectImpl 
  implements GraphObject {
  
  private static Logger logger = Logger.getLogger(GraphObject.class);
  
  private List data;
  
  private Identity identity;
  
  public AbstractGraphObjectImpl(Identity identity) {
    this.identity = identity;
    this.data = new LinkedList();
  }


  public List getData() {
    return data;
  }
  
  /*  (non-Javadoc)
   * @see entrospector.graph.interfaces.GraphObject#addData(entrospector.graph.interfaces.Data)
   */
  public void addData(Data data) {
    this.data.add(data);
  }
  /*  (non-Javadoc)
   * @see entrospector.graph.interfaces.GraphObject#removeData(entrospector.graph.interfaces.Data)
   */
  public boolean removeData(Data data) {
    return this.data.remove(data);
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.GraphObject#setIdentity(entrospector.Identity)
   */
  public void setIdentity(Identity id) {
    identity = id;
  }
  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.GraphObject#getIdentity()
   */
  public Identity getIdentity() {
    return identity;
  }
  public boolean equals(Object that) {
    boolean result = (compareTo(that) == 0);
    if(logger.isDebugEnabled()){
      logger.debug(this+".equals("+that+") = " + result);
    }
    return result;
  }
  public int compareTo(Object that) {
    return getIdentity().compareTo(((GraphObject)that).getIdentity());
  }
  public int hashCode() {
    if(getIdentity() != null) {
      return getIdentity().hashCode();
    }else {
      return super.hashCode();
    }
  }  
  public Object clone() {
    try {
      return super.clone();
    } catch (CloneNotSupportedException e) {
      throw new InternalError();
    }
  }
}
