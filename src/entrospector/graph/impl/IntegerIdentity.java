/*
 * Created on Mar 7, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package entrospector.graph.impl;

import org.apache.log4j.Logger;

import entrospector.graph.interfaces.Identity;


/**
 * @author rwinch
 */
public class IntegerIdentity extends AbstractIdentity {
  private static final Logger logger = Logger.getLogger(IntegerIdentity.class);
  
  public IntegerIdentity(Integer id) {
    super(id);
  }
  public IntegerIdentity(String id) {    
    this(Integer.parseInt(id));
  }
  public IntegerIdentity(int id) {
    this(new Integer(id));
  }  
  public Identity next() {
    return new IntegerIdentity(((Integer)getIdentity()).intValue()+1);
  }  
  public int compareTo(Object that) {
    if(logger.isDebugEnabled()) {
      logger.debug(this+".compareTo("+that+")");
    }
    if(that instanceof IntegerIdentity) {
      IntegerIdentity thatIntId = (IntegerIdentity)that;
      return getIdentity().compareTo(thatIntId.getIdentity());
    }else if(that instanceof Integer) {
      return getIdentity().compareTo(that);
    }else {
      return 1;
    }
  }
}
