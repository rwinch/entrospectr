/*
 * Created on Mar 7, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package entrospector.graph.impl;

import org.apache.log4j.Logger;

import entrospector.graph.interfaces.Identity;


/**
 * @author rwinch
 */
public class StringIdentity extends AbstractIdentity {
  private static final Logger logger = Logger.getLogger(StringIdentity.class);
  
  public StringIdentity(String id) {
    super(id);    
  }  
  public Identity next() {
    if(logger.isDebugEnabled()) {
      logger.debug(this+".next()");
    }
    
    StringIdentity result = null;
    String id = this.toString();
    
    char[] array = id.toCharArray();
    
    int index = array.length-1;
    char current = ' ';
    while((index >= 0) && (index <= array.length-1)) {
      logger.info("start while");
      current = array[index];
      if(current == '.') {        
        logger.info("at .");
        array[index] = '0';
      }else {
        logger.info("not .");
        array[index] += 1;
        break;
      }      
      index--;
      logger.info("end while");
    }
    logger.info("done loop");
    
    String strId = new String(array);
    
    if(array == null || array.length <=0) {
      strId = "0";
    }else if(current == '.') {
      logger.info("appending");
      strId = "0"+strId;
    }
    
    result = new StringIdentity(strId);
    
    return result;
  }    
  public int compareTo(Object that) {
    if(that instanceof StringIdentity) {
      return getIdentity().compareTo(((StringIdentity)that).getIdentity());
    }else if (that instanceof String) {
      return getIdentity().compareTo(that);
    }else {
      return -1;
    }
  }
}
