/*
 *
 */
package entrospector.graph.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import entrospector.graph.interfaces.Data;
import entrospector.graph.interfaces.Edge;
import entrospector.graph.interfaces.Graph;
import entrospector.graph.interfaces.Identity;
import entrospector.graph.interfaces.Vertex;

/**
 * @author rwinch
 */
public class UndirectedGraphImpl 
  extends AbstractGraphObjectImpl implements Graph {
  
  private Logger logger = Logger.getLogger(UndirectedGraphImpl.class);
   
  /**
   * This contains the vertex and the incoming and outgoing edges associated w/ this node
   * @author rwinch
   */
  protected class GraphNode extends AbstractGraphObjectImpl {
    private Vertex vertex;
    private Collection incomingEdges = new LinkedList();
    private Collection outgoingEdges = new LinkedList();
    private Collection selfEdges = new LinkedList();
    
    public GraphNode(Vertex v) {
      super(v.getIdentity());
      this.vertex = v;
    }
    public Vertex getVertex() {
      return vertex;
    }
    
    public void addIncomingEdge(Edge edge) {
      incomingEdges.add(edge);
    }
    public boolean removeIncomingEdge(Edge edge) {
      return incomingEdges.remove(edge);
    }
    public boolean removeSelfEdge(Edge edge) {
      return selfEdges.remove(edge);
    }    
    public void addOutgoingEdge(Edge edge) {
      outgoingEdges.add(edge);
    }
    public void addSelfEdge(Edge edge) {
      selfEdges.add(edge);
    }
    public boolean removeOutgoingEdge(Edge edge) {
      return outgoingEdges.remove(edge);
    }
    
    public Collection getAllEdges() {
      Collection result = new LinkedList();
      if(logger.isDebugEnabled()) { 
      	logger.debug("getOutgoing: "+getOutgoingEdges());
      }
      result.addAll(getOutgoingEdges());
      if(logger.isDebugEnabled()) {
        logger.debug("getIncoming: "+getIncomingEdges());
      }
      result.addAll(getIncomingEdges());
      if(logger.isDebugEnabled()) {
        logger.debug("getSelf: "+getSelfEdges());
      }
      result.addAll(getSelfEdges());
      return result;
    }
    public Collection getIncomingEdges() {
      return incomingEdges;
    }
    public Collection getOutgoingEdges() {
      return outgoingEdges;
    }
    public Collection getSelfEdges() {
      return selfEdges;
    }
    public int compareTo(Object that) {
      return getVertex().compareTo(that);     
    }    
    public boolean equals(Object that) {
      return compareTo(that) == 0;
    }   
    public String toString() {
      return "{"+String.valueOf(vertex)+", in="+incomingEdges+", out="+outgoingEdges+", self="+selfEdges+"}";    
    }
  }
  
  private SortedSet graphNodes = new TreeSet();
  
  public UndirectedGraphImpl() {
    this(null);
  }
  
  /**
   * @param identity
   */
  public UndirectedGraphImpl(Identity identity) {
    super(identity);
  }
  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#addEdge(entrospector.graph.interfaces.Edge)
   */
  public void addEdge(Edge edge) {
    // TODO: Should we clone edge
    // what are our implications
    
    GraphNode sourceNode, destinationNode;
    
    Vertex source = edge.getA();
    Vertex destination = edge.getB();
    
    if(!graphNodes.contains(source)) {
      if(logger.isDebugEnabled()) {
        logger.debug("doesn't contain source "+source);
      }
      sourceNode = new GraphNode(source);      
      graphNodes.add(sourceNode);
      if(logger.isDebugEnabled()) {
        logger.debug("graphNodes="+graphNodes);          
      }
    }else {
      if(logger.isDebugEnabled()) {
      	logger.debug("does contain source "+source);
      }
      sourceNode = getGraphNode(source);
    }
    
    if(source.equals(destination)) {
      destinationNode = sourceNode;
      sourceNode.addSelfEdge(edge);
      if(logger.isDebugEnabled()) {
        logger.debug("SelfEdge "+getGraphNode(source).getSelfEdges());
      }
    }else {	    
	    if(!graphNodes.contains(destination)) {
	      if(logger.isDebugEnabled()) {
	      	logger.debug("doesn't contain destination "+destination);
	      }
	      destinationNode = new GraphNode(destination);      
	      graphNodes.add(destinationNode);
	    }else {
	      if(logger.isDebugEnabled()) {
	        logger.debug("does contain destination "+destination);
	      }
	      destinationNode = getGraphNode(destination);      
	    }	    
	    sourceNode.addOutgoingEdge(edge);
	    destinationNode.addIncomingEdge(edge);
    }
    if(logger.isDebugEnabled()) {
      logger.debug("sourceNode="+destinationNode+", destinationNode"+destinationNode);
    }
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#addEdges(java.util.Collection)
   */
  public void addEdges(Collection edges) {
    Iterator iEdges = edges.iterator();
    while(iEdges.hasNext()) {
      Edge edgeObject = (Edge)iEdges.next();
      this.addEdge(edgeObject);
    }
  }
  
  public void addGraphs(Collection graphs) {
    Iterator iGraphs = graphs.iterator();
    while(iGraphs.hasNext()) {
      Graph graphObject = (Graph)iGraphs.next();
      this.addGraph(graphObject);
    }
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#addGraph(entrospector.graph.interfaces.Graph)
   */
  public void addGraph(Graph g) {
    this.addEdges(g.getEdges());
    this.addVertices(g.getVertices());
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#addVertex(entrospector.graph.interfaces.Vertex)
   */
  public boolean addVertex(Vertex vertex) {
    return graphNodes.add(new GraphNode(vertex));
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#addVertices(java.util.Collection)
   */
  public void addVertices(Collection vertices) {
    Iterator iVertices = vertices.iterator();
    while(iVertices.hasNext()) {
      Vertex vertex = (Vertex)iVertices.next();
      this.addVertex(vertex);
    }
  }
  
  public void addDataFor(Data data, Vertex vertex) {
    GraphNode graphNode = this.getGraphNode(vertex);
    if(graphNode!=null) {
      graphNode.addData(data);
    }
  }
  
  public void removeDataFor(Data data, Vertex vertex) {
    GraphNode graphNode = this.getGraphNode(vertex);
    if(graphNode!=null) {
    	graphNode.removeData(data);
    }
  }
  public List getDataFor(Vertex vertex) {
    GraphNode graphNode = this.getGraphNode(vertex);
    if(graphNode!=null) {
      return graphNode.getData();
    }else {
      return null;
    }
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#removeEdge(entrospector.graph.interfaces.Edge)
   */
  public boolean removeEdge(Edge edge) {
    if(logger.isDebugEnabled()) {
      logger.debug("removeEdge ("+edge+")");
    }
    
    GraphNode sourceNode = getGraphNode(edge.getB());
    if(logger.isDebugEnabled()) {
      logger.debug("sourceNode = "+sourceNode);
    }
    if(sourceNode == null) {      
      return false;
    }
           
    if(edge.getB() != null && edge.getB().equals(edge.getA())) {
      logger.info("returning remove self");
      return sourceNode.removeSelfEdge(edge);
    }else {
      GraphNode destinationNode = getGraphNode(edge.getA());
      if(logger.isDebugEnabled()){
        logger.debug("destinationNode = "+destinationNode);
      }
      if(destinationNode == null) {
        return false;
      }
      boolean resultSource = sourceNode.removeOutgoingEdge(edge);
      boolean resultDestination = destinationNode.removeIncomingEdge(edge);
      if(resultSource && resultDestination) {
        logger.info("source.out dest.in");
        return true;
      }else if(resultSource || resultDestination) {
        // we successfully removed one without the other, this should be impossible
        throw new RuntimeException("Error: Edges weren't synchronized (first) '"+edge+"'");
      }else {
        resultSource = sourceNode.removeIncomingEdge(edge);
        resultDestination = destinationNode.removeOutgoingEdge(edge);
        if(resultSource && resultDestination) {
          logger.info("source.in destination.out");
          return true;
        }else if(resultSource || resultDestination) {
          // we successfully removed one without the other, this should be impossible
          throw new RuntimeException("Error: Edges weren't synchronized (second) '"+edge+"'");
        }else {
          return false;
        }
      }      
    }
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#removeEdges(java.util.Collection)
   */
  public void removeEdges(Collection edges) {
    Iterator iEdges = edges.iterator();
    while(iEdges.hasNext()) {
      Edge edgeObj = (Edge) iEdges.next();
      removeEdge(edgeObj);
    }
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#removeGraph(entrospector.graph.interfaces.Graph)
   */
  public void removeGraph(Graph g) {
    removeVertices(g.getVertices());
  }

  /*  (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#removeVertex(entrospector.graph.interfaces.Vertex)
   */
  public boolean removeVertex(Vertex vertex) {
    removeEdges(getEdgesOf(vertex));
    return graphNodes.remove(vertex);
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#removeVertices(java.util.Collection)
   */
  public void removeVertices(Collection vertices) {
    Iterator iVertices = vertices.iterator();
    while(iVertices.hasNext()) {
      Vertex v = (Vertex)iVertices.next();
      removeVertex(v);
    }
  }
  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#reset()
   */
  public void reset() {
    this.graphNodes = new TreeSet();
  }
  
  /*  (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#getDegreeOf(entrospector.graph.interfaces.Vertex)
   */
  public int getDegreeOf(Vertex vertex) {
    return getEdgesOf(vertex).size();
  }
    
  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#getEdges()
   */
  public Collection getEdges() {    
    Collection result = new LinkedList();
    Iterator iNodes = graphNodes.iterator();
    while(iNodes.hasNext()) {
      GraphNode node = ((GraphNode)iNodes.next());
      Collection outEdges = node.getOutgoingEdges();    
      result.addAll(outEdges);
      Collection selfEdges = node.getSelfEdges();
      if(logger.isDebugEnabled()){
        logger.debug("selfEdges["+node.getVertex()+"]="+selfEdges);
      }
      result.addAll(selfEdges);
    }
    
    return result;
  }
  /*  (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#getSelfEdges()
   */
  public Collection getSelfEdges() {
    Collection result = new LinkedList();
    Iterator iNodes = graphNodes.iterator();
    while(iNodes.hasNext()) {
      GraphNode node = ((GraphNode)iNodes.next());      
      Collection selfEdges = node.getSelfEdges();
      if(logger.isDebugEnabled()) {
        logger.debug("selfEdges["+node.getVertex()+"]="+selfEdges);
      }
      result.addAll(selfEdges);
    }
    
    return result;
  }
  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#getVertex(entrospector.graph.interfaces.Vertex)
   */
  public Vertex getVertex(Vertex vertex) {
    GraphNode node = getGraphNode(vertex);
    Vertex result = node.getVertex();
    if(result == null) {
      throw new RuntimeException("Node: '"+vertex+"' doesn't exist");
    }
    return result;
  }
  /*  (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#getVertex(java.lang.String)
   */
  public Vertex getVertex(String id) {
    return getVertex(new VertexImpl(IDFactory.create(id)));
  }
  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#getVertices()
   */
  public Collection getVertices() {
    Collection result = new LinkedList();
    Iterator iNodes = graphNodes.iterator();
    
    while(iNodes.hasNext()) {
      Vertex v = ((GraphNode)iNodes.next()).getVertex();
      result.add(v);
    }
    
    return result;
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#getNeighborsOf(entrospector.graph.interfaces.Vertex)
   */
  public Collection getNeighborsOf(Vertex vertex) {
    if(logger.isDebugEnabled()) {
      logger.debug("getNeighborsOf( "+vertex+" )");
    }
    Collection result = new LinkedList();
             
    Iterator iEdges = getEdgesOf(vertex).iterator();
    while(iEdges.hasNext()) {
      Edge edge = (Edge)iEdges.next();
      if(logger.isDebugEnabled()) {
      	logger.debug("edge = "+edge);
      }
      Vertex source = edge.getB();
      Vertex destination = edge.getA();
      
      boolean isSource = vertex.equals(source);
      boolean isDestination = vertex.equals(destination);
      
      if(!isSource && isDestination) {
        logger.info("added source");
        result.add(source);
      }else if(isSource && !isDestination){
        logger.info("added destination");
        result.add(destination);
      }else if(isSource && isDestination) {
        logger.info("added source (self)");
        result.add(source); // it is a self loop
      }else {
        // ERROR, never should get here, means that an edge that doesn't belong got associate w/ this vertex
        throw new RuntimeException("Something is BROKE! Vertex '"+vertex+"' cant have edge '"+edge+"'"); 
      }
    }
    return result;
  }
  
  /*  (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#getSelfEdgesOf(entrospector.graph.interfaces.Vertex)
   */
  public Collection getSelfEdgesOf(Vertex vertex) {
    Collection result = new LinkedList();
    GraphNode node = getGraphNode(vertex);
    result.addAll(node.getSelfEdges());
    return result;
  }
  /*  (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#getDestinationEdgesOf(entrospector.graph.interfaces.Vertex)
   */
  public Collection getDestinationEdgesOf(Vertex vertex) {
    Collection result = new LinkedList();
    GraphNode node = getGraphNode(vertex);
    result.addAll(node.getIncomingEdges());
    return result;
  }
  /*  (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#getSourceEdgesOf(entrospector.graph.interfaces.Vertex)
   */
  public Collection getSourceEdgesOf(Vertex vertex) {
    Collection result = new LinkedList();
    GraphNode node = getGraphNode(vertex);
    result.addAll(node.getOutgoingEdges());
    return result;
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#getEdgesOf(entrospector.graph.interfaces.Vertex)
   */
  public Collection getEdgesOf(Vertex vertex) {
    if(logger.isDebugEnabled()) {
      logger.debug("getEdgesOf ("+vertex+")");
    }
    Collection result = new LinkedList();
    GraphNode node = getGraphNode(vertex);
    result.addAll(node.getAllEdges());
    if(logger.isDebugEnabled()) {
      logger.debug("result="+result);
    }
    return result;
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#isVertexPresent(entrospector.graph.interfaces.Vertex)
   */
  public boolean isVertexPresent(Vertex vertex) {
    return graphNodes.contains(vertex);
  }

  /* (non-Javadoc)
   * @see entrospector.graph.interfaces.Graph#isEdgePresent(entrospector.graph.interfaces.Edge)
   */
  public boolean isEdgePresent(Edge edge) {
    GraphNode node = getGraphNode(edge.getB());
    return node.getAllEdges().contains(edge);
  }

  /* (non-Javadoc)
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  public int compareTo(Object that) {    
    Graph thatGraph = (Graph)that;
    int result = this.getVertices().size() - thatGraph.getVertices().size();
    if(result == 0) {
      result = this.getEdges().size() - thatGraph.getEdges().size();
    }
    return result;
  }
  /* (non-Javadoc)
   * @see java.lang.Object#clone()
   */
  public Object clone() {
    Graph result = (Graph)super.clone();
    result.reset();
    result.addEdges(this.getEdges());
    result.addVertices(this.getVertices());
    return result;
  }
  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return this.graphNodes.toString();
  }
  
  /////// private methods /////////
  private GraphNode getGraphNode(Vertex v) {
    if(logger.isDebugEnabled()) {
      logger.debug("getNode("+v+")");
    }
    GraphNode fTailSet = (GraphNode) graphNodes.tailSet(v).first();
    if(!v.equals(fTailSet)) {
      fTailSet = null;
    }
    return fTailSet;
  }
}
