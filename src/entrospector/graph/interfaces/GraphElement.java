/*
 * 
 */
package entrospector.graph.interfaces;

/**
 * @author rwinch
 */
public interface GraphElement
extends GraphObject
{
   /**
    * calls <code>g.isVertexPresent(this);</code>
    * @param g
    * @return
    */
    boolean isPresentIn(Graph g);
}