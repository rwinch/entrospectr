/*
 * 
 */
package entrospector.graph.interfaces;

import java.util.Collection;
import java.util.List;

/**
 * @author rwinch
 */
public interface Graph extends GraphObject {
  /**
   * Adds data for this graph for a particular vertex 
   * @param data
   * @param vertex
   */
  public void addDataFor(Data data, Vertex vertex);

  /**
   * Removes data for this graph for a particular vertex
   * @param data
   * @param vertex
   */
  public void removeDataFor(Data data, Vertex vertex);
  /**
   * Gets data for this graph for a particular vertex
   * @param vertex
   * @return
   */
  public List getDataFor(Vertex vertex);

  /**
   * Adds an edge to this graph
   * 
   * @param e -
   *          the edge to add
   */
  void addEdge(Edge e);

  /**
   * Adds a <code>Collection</code> of edges to the graph
   * 
   * @param edges
   */
  void addEdges(Collection edges);

  /**
   * Adds a <code>Graph</code> to this Graph
   * 
   * @param g
   */
  void addGraph(Graph g);

  /**
   * Adds a <code>Collection</code> of <code>Graph</code>'s to this
   * <code>Graph</code>
   * 
   * @param edges
   */
  void addGraphs(Collection edges);

  /**
   * Adds a <code>Vertex</code> to this <code>Graph</code>
   * 
   * @param vertex
   * @return
   */
  boolean addVertex(Vertex vertex);

  /**
   * Adds a <code>Collection</code> of <code>Vertex</code>'s to this
   * <code>Graph</code>
   * 
   * @param vertices
   */
  void addVertices(Collection vertices);

  /**
   * Removes an <code>Edge</code> from this <code>Graph</code>
   * 
   * @param e
   * @return
   */
  boolean removeEdge(Edge e);

  /**
   * Removes a <code>Collection</code> of <code>Edge</code>'s from this
   * <code>Graph</code>
   * 
   * @param edges
   */
  void removeEdges(Collection edges);

  /**
   * Removes a <code>Graph</code> from this <code>Graph</code>
   * 
   * @param g
   */
  void removeGraph(Graph g);

  /**
   * Removes a <code>Vertex</code> from this <code>Graph</code>
   * 
   * @param vertex
   * @return
   */
  boolean removeVertex(Vertex vertex);

  /**
   * Removes a <code>Collection</code> of <code>Vertex</code>'s from this
   * <code>Graph</code>
   * 
   * @param vertices
   */
  void removeVertices(Collection vertices);

  /**
   * Resets this graph (removes all vertices and edges)
   */
  void reset();

  /**
   * Gets a <code>Collection</code> of edges associated with this
   * <code>Graph</code>
   * 
   * @return
   */
  Collection getEdges();

  /**
   * Gets a <code>Collection</code> of self-edges associated with this
   * <code>Graph</code>
   * 
   * @return
   */
  Collection getSelfEdges();

  /**
   * Gets a <code>Vertex</code> that has this id from this <code>Graph</code>
   * 
   * @param id
   * @return
   */
  Vertex getVertex(String id);

  /**
   * Gets a <code>Vertex</code> that equals <code>v</code> from this
   * <code>Graph</code>
   * 
   * @param v
   * @return
   */
  Vertex getVertex(Vertex v);

  /**
   * Gets a <code>Collection</code> of <code>Vertex</code>'s
   * 
   * @return
   */
  Collection getVertices();

  /**
   * Gets a <code>Collection</code> of a <code>Vertex</code>'s neighbors
   * 
   * @param vertex
   * @return
   */
  Collection getNeighborsOf(Vertex vertex);

  /**
   * Gets a <code>Collection</code> of <code>Vertex</code>'s
   * <code>Edge</code>'s
   * 
   * @param vertex
   * @return
   */
  Collection getEdgesOf(Vertex vertex);

  /**
   * Gets a <code>Collection</code> of <code>Vertex</code>'s self-
   * <code>Edge</code>'s
   * 
   * @param vertex
   * @return
   */
  Collection getSelfEdgesOf(Vertex vertex);

  /**
   * Returns a <code>Collection</code> of the <code>Edge</code>'s that
   * <code>vertex</code> is the destination of and not the source of
   * 
   * @param vertex
   * @return
   */
  Collection getDestinationEdgesOf(Vertex vertex);

  /**
   * Returns a <code>Collection</code> of the <code>Edge</code>'s that
   * <code>vertex</code> is the source of and not the destination of
   * 
   * @param vertex
   * @return
   */
  Collection getSourceEdgesOf(Vertex vertex);

  /**
   * Get the degree of a <code>Vertex</code> for this <code>Graph</code>
   * 
   * @param vertex
   * @return
   */
  int getDegreeOf(Vertex vertex);

  /**
   * Determines if a <code>Vertex</code> is present in this <code>Graph</code>
   * 
   * @param vertex
   * @return
   */
  boolean isVertexPresent(Vertex vertex);

  /**
   * Determines if an <code>Edge</code> is present in this <code>Graph</code>
   * @param edge
   * @return
   */
  boolean isEdgePresent(Edge edge);

}