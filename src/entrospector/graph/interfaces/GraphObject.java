/*
 * 
 */
package entrospector.graph.interfaces;

import java.util.List;


/**
 * @author rwinch
 */
public interface GraphObject 
  extends Cloneable, Comparable
{
	// accessor
	List getData();
	// modifiers
	void addData(Data data);
	boolean removeData(Data data);
	void setIdentity(Identity id);
	Identity getIdentity();
	Object clone();
}