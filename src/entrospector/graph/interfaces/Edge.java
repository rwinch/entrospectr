/*
 * 
 */
package entrospector.graph.interfaces;

/**
 * @author rwinch
 */
public interface Edge
extends GraphElement
{
    Vertex getA();
	Vertex getB();
}