/*
 * Created on Mar 7, 2005
 */
package entrospector.graph.interfaces;

/**
 * @author rwinch
 */
public interface Identity extends Comparable {
  /**
   * Returns the identity after this one
   * @return
   */
  public Identity next();
  /**
   * The internal object that represents the identity
   * @return
   */
  public Comparable getIdentity();
}
